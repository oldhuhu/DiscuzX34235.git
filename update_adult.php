<?php

// 不升级到 InnoDB 开关
// 仅限极少量特殊场合应用
define("NO_INNODB_FEATURE", false);
// 数据表改名开关
// 仅限极少量特殊场合应用
define("NO_RENAME_TABLE", true);
define('UPGRADE_LOG_PATH', __DIR__.'/../data/log/X3.5_upgrade.php');
empty($_GET['css']) || cssoutput();

@set_time_limit(0);
@ignore_user_abort(TRUE);
ini_set('max_execution_time', 0);
ini_set('mysql.connect_timeout', 0);

include_once('../source/class/class_core.php');
include_once('../source/function/function_core.php');
include_once('../source/function/function_cache.php');

$cachelist = array();
$discuz = C::app();

$discuz->cachelist = $cachelist;
$discuz->init_cron = false;
$discuz->init_setting = true;
$discuz->init_user = false;
$discuz->init_session = false;
$discuz->init_misc = false;

$discuz->init();

header("Content-type: text/html; charset=utf-8");

$config = array(
	'dbcharset' => $_G['config']['db']['1']['dbcharset'],
	'charset' => $_G['config']['output']['charset'],
	'tablepre' => $_G['config']['db']['1']['tablepre']
);

$step = !empty($_GET['step']) && in_array($_GET['step'], array('welcome', 'tips', 'license', 'envcheck', 'confirm', 'config', 'innodb', 'scheme', 'utf8mb4', 'serialize', 'serialize_plugin', 'file', 'dataupdate')) ? $_GET['step'] : 'welcome';
$theurl = htmlspecialchars($_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME']);
$lockfile = DISCUZ_ROOT.'./data/update.lock';

timezone_set();

// PHP 8 拦截
if (version_compare(PHP_VERSION, '8.0.0', '>=')) {
	show_msg('升级程序支持的 PHP 版本为 5.6 - 7.4 ，请先降级 PHP 至 7.4 再升级 Discuz! X 程序，程序升级完成后可以使用 PHP 8.0 或更高版本。');
}

// Q004 保证无写入权限时可以正常报错
// https://www.dismall.com/thread-14718-1-1.html
if (!@touch(UPGRADE_LOG_PATH) || @!is_writable(UPGRADE_LOG_PATH)) {
	show_msg('请您先登录服务器，给 Discuz! X 目录下的 data/log 目录赋予写权限，随后再次运行本文件进行升级。');
}

$tables = array();

$sys_tables_not_full = array('pre_common_admincp_cmenu', 'pre_common_admincp_group', 'pre_common_admincp_member', 'pre_common_admincp_perm', 'pre_common_admincp_session', 'pre_common_admingroup', 'pre_common_adminnote', 'pre_common_advertisement', 'pre_common_advertisement_custom', 'pre_common_banned', 'pre_common_block', 'pre_common_block_favorite', 'pre_common_block_item', 'pre_common_block_item_data', 'pre_common_block_permission', 'pre_common_block_pic', 'pre_common_block_style', 'pre_common_block_xml', 'pre_common_cache', 'pre_common_card', 'pre_common_card_log', 'pre_common_card_type', 'pre_common_connect_guest', 'pre_common_credit_log', 'pre_common_credit_log_field', 'pre_common_credit_rule', 'pre_common_credit_rule_log', 'pre_common_credit_rule_log_field', 'pre_common_cron', 'pre_common_devicetoken', 'pre_common_district', 'pre_common_diy_data', 'pre_common_domain', 'pre_common_failedip', 'pre_common_failedlogin', 'pre_common_friendlink', 'pre_common_grouppm', 'pre_common_invite', 'pre_common_magic', 'pre_common_magiclog', 'pre_common_mailcron', 'pre_common_mailqueue', 'pre_common_member', 'pre_common_member_action_log', 'pre_common_member_connect', 'pre_common_member_count', 'pre_common_member_crime', 'pre_common_member_field_forum', 'pre_common_member_field_home', 'pre_common_member_forum_buylog', 'pre_common_member_grouppm', 'pre_common_member_log', 'pre_common_member_magic', 'pre_common_member_medal', 'pre_common_member_newprompt', 'pre_common_member_profile', 'pre_common_member_profile_setting', 'pre_common_member_security', 'pre_common_member_secwhite', 'pre_common_member_stat_field', 'pre_common_member_status', 'pre_common_member_validate', 'pre_common_member_verify', 'pre_common_member_verify_info', 'pre_common_myapp', 'pre_common_myinvite', 'pre_common_mytask', 'pre_common_nav', 'pre_common_onlinetime', 'pre_common_optimizer', 'pre_common_patch', 'pre_common_plugin', 'pre_common_pluginvar', 'pre_common_process', 'pre_common_regip', 'pre_common_relatedlink', 'pre_common_remote_port', 'pre_common_report', 'pre_common_searchindex', 'pre_common_seccheck', 'pre_common_secquestion', 'pre_common_session', 'pre_common_setting', 'pre_common_smiley', 'pre_common_sphinxcounter', 'pre_common_stat', 'pre_common_statuser', 'pre_common_style', 'pre_common_stylevar', 'pre_common_syscache', 'pre_common_tag', 'pre_common_tagitem', 'pre_common_task', 'pre_common_taskvar', 'pre_common_template', 'pre_common_template_block', 'pre_common_template_permission', 'pre_common_uin_black', 'pre_common_usergroup', 'pre_common_usergroup_field', 'pre_common_visit', 'pre_common_word', 'pre_common_word_type', 'pre_connect_disktask', 'pre_connect_feedlog', 'pre_connect_memberbindlog', 'pre_connect_postfeedlog', 'pre_connect_tthreadlog', 'pre_forum_access', 'pre_forum_activity', 'pre_forum_activityapply', 'pre_forum_announcement', 'pre_forum_attachment', 'pre_forum_attachment_0', 'pre_forum_attachment_1', 'pre_forum_attachment_2', 'pre_forum_attachment_3', 'pre_forum_attachment_4', 'pre_forum_attachment_5', 'pre_forum_attachment_6', 'pre_forum_attachment_7', 'pre_forum_attachment_8', 'pre_forum_attachment_9', 'pre_forum_attachment_exif', 'pre_forum_attachment_unused', 'pre_forum_attachtype', 'pre_forum_bbcode', 'pre_forum_collection', 'pre_forum_collectioncomment', 'pre_forum_collectionfollow', 'pre_forum_collectioninvite', 'pre_forum_collectionrelated', 'pre_forum_collectionteamworker', 'pre_forum_collectionthread', 'pre_forum_creditslog', 'pre_forum_debate', 'pre_forum_debatepost', 'pre_forum_faq', 'pre_forum_filter_post', 'pre_forum_forum', 'pre_forum_forum_threadtable', 'pre_forum_forumfield', 'pre_forum_forumrecommend', 'pre_forum_groupcreditslog', 'pre_forum_groupfield', 'pre_forum_groupinvite', 'pre_forum_grouplevel', 'pre_forum_groupuser', 'pre_forum_hotreply_member', 'pre_forum_hotreply_number', 'pre_forum_imagetype', 'pre_forum_medal', 'pre_forum_medallog', 'pre_forum_memberrecommend', 'pre_forum_moderator', 'pre_forum_modwork', 'pre_forum_newthread', 'pre_forum_onlinelist', 'pre_forum_order', 'pre_forum_poll', 'pre_forum_polloption', 'pre_forum_polloption_image', 'pre_forum_pollvoter', 'pre_forum_post', 'pre_forum_post_location', 'pre_forum_post_moderate', 'pre_forum_post_tableid', 'pre_forum_postcache', 'pre_forum_postcomment', 'pre_forum_postlog', 'pre_forum_poststick', 'pre_forum_promotion', 'pre_forum_ratelog', 'pre_forum_relatedthread', 'pre_forum_replycredit', 'pre_forum_rsscache', 'pre_forum_sofa', 'pre_forum_spacecache', 'pre_forum_statlog', 'pre_forum_thread', 'pre_forum_thread_moderate', 'pre_forum_threadaddviews', 'pre_forum_threadcalendar', 'pre_forum_threadclass', 'pre_forum_threadclosed', 'pre_forum_threaddisablepos', 'pre_forum_threadhidelog', 'pre_forum_threadhot', 'pre_forum_threadimage', 'pre_forum_threadlog', 'pre_forum_threadmod', 'pre_forum_threadpartake', 'pre_forum_threadpreview', 'pre_forum_threadprofile', 'pre_forum_threadprofile_group', 'pre_forum_threadrush', 'pre_forum_threadtype', 'pre_forum_trade', 'pre_forum_tradecomment', 'pre_forum_tradelog', 'pre_forum_typeoption', 'pre_forum_typeoptionvar', 'pre_forum_typevar', 'pre_forum_warning', 'pre_home_album', 'pre_home_album_category', 'pre_home_appcreditlog', 'pre_home_blacklist', 'pre_home_blog', 'pre_home_blog_category', 'pre_home_blog_moderate', 'pre_home_blogfield', 'pre_home_class', 'pre_home_click', 'pre_home_clickuser', 'pre_home_comment', 'pre_home_comment_moderate', 'pre_home_docomment', 'pre_home_doing', 'pre_home_doing_moderate', 'pre_home_favorite', 'pre_home_feed', 'pre_home_feed_app', 'pre_home_follow', 'pre_home_follow_feed', 'pre_home_follow_feed_archiver', 'pre_home_friend', 'pre_home_friend_request', 'pre_home_friendlog', 'pre_home_notification', 'pre_home_pic', 'pre_home_pic_moderate', 'pre_home_picfield', 'pre_home_poke', 'pre_home_pokearchive', 'pre_home_share', 'pre_home_share_moderate', 'pre_home_show', 'pre_home_specialuser', 'pre_home_userapp', 'pre_home_userappfield', 'pre_home_visitor', 'pre_mobile_setting', 'pre_mobile_wsq_threadlist', 'pre_portal_article_content', 'pre_portal_article_count', 'pre_portal_article_moderate', 'pre_portal_article_related', 'pre_portal_article_title', 'pre_portal_article_trash', 'pre_portal_attachment', 'pre_portal_category', 'pre_portal_category_permission', 'pre_portal_comment', 'pre_portal_comment_moderate', 'pre_portal_rsscache', 'pre_portal_topic', 'pre_portal_topic_pic', 'pre_security_evilpost', 'pre_security_eviluser', 'pre_security_failedlog', 'pre_common_member_archive', 'pre_common_member_profile_archive', 'pre_common_member_field_forum_archive', 'pre_common_member_field_home_archive', 'pre_common_member_status_archive', 'pre_common_member_count_archive', 'pre_common_member_wechat', 'pre_mobile_wechat_authcode', 'pre_common_member_wechatmp', 'pre_mobile_wsq_threadlist', 'pre_mobile_wechat_resource', 'pre_mobile_wechat_masssend');

$scheme_count = 0;

$tablescachename = 'update_tables';
$tablescachefile = DISCUZ_ROOT . './data/sysdata/cache_' . $tablescachename . '.php';
if (in_array($_GET['step'], array('innodb', 'utf8mb4', 'serialize', 'serialize_plugin'))) {
	if (!file_exists($tablescachefile)) {
		$db_result = DB::fetch_all('SHOW TABLE STATUS WHERE `Name` LIKE \''.$config['tablepre'].'%\';');
		foreach ($db_result as $tb) {
			if (strstr($tb['Name'], $config['tablepre'].'ucenter_') || strstr($tb['Name'], $config['tablepre'].'uc_') || strstr($tb['Name'], $config['tablepre'].'forum_postposition')) {
				continue;
			}
			$tables = array_merge($tables, array(str_replace($config['tablepre'], 'pre_', $tb['Name'])));
		}
		logmessage("write " . str_replace(DISCUZ_ROOT, '', $tablescachefile) . "\t" . count($tables) . ".");
		writetocache($tablescachename, getcachevars(array('tables' => $tables)));
	} else {
		@include_once $tablescachefile;
	}

	if (empty($tables)) {
		logmessage("show table error.");
		show_msg('无法获取数据库表');
	}

	$table = empty($_GET['table']) ? $tables[0] : $_GET['table'];
	if (!in_array($table, $tables)) {
		logmessage("table name error.");
		show_msg('数据库表名不合法');
	}
}

logmessage("<?php exit;?>\t".date("Y-m-d H:i:s"));
logmessage("Query String: ".$_SERVER['QUERY_STRING']);

if (!empty($_GET['lock'])) {
	@touch($lockfile);
	@unlink($theurl);
	logmessage("upgrade success.");
	show_msg('恭喜，您已经成功升级到 Discuz! X3.5 版本，感谢您的使用！');
}

if (file_exists($lockfile)) {
	logmessage("upgrade locked.");
	show_msg('请您先登录服务器，手工删除 data/update.lock 文件，再次运行本文件进行升级。');
}

if ($step == 'welcome') {

	show_msg('<p class="lead">Discuz! X3.5 在继承和完善 Discuz! X3.4 宗旨的基础上， Discuz! 社区以“不忘初心”为主线，针对“系统安全”、“IPv6”、“运营拓展”、“负载性能”、“用户体验”和“管理体验”几大方面，全面优化和打造，多项功能改进。本程序将不定期的在官方 Git 中更新，欲追求更新版本的站长欢迎关注。</p><p class="lead">本程序将引导您从 Discuz! X3.4 升级至 Discuz! X3.5 ，请在升级之前做好站点全量数据（含数据库、文件）的备份操作，并小心操作。</p><p class="lead">在您准备好进行操作之后，点击“下一步”按钮继续操作。</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=tips\';">下一步 ></button></p>', '欢迎您执行 Discuz! X3.5 升级程序', 1);

} else if ($step == 'tips') {

	show_msg('<p class="lead">本程序将引导您从 Discuz! X3.4 升级至 Discuz! X3.5 ，请在升级之前做好站点全量数据（含数据库、文件）的备份操作，并小心操作。同时建议既有站点升级前将 MySQL 升级至 5.7 及以上版本，以避免插件建表索引过长导致升级中断。</p><p class="lead">由于 UCenter 1.7.0 更新了数据库编码，对于您当前数据库内不支持 utf8mb4_unicode_ci 编码的用户名，UCenter 程序会将用户重命名为 15 位随机字符，请您自行在升级成功后在 UCenter 后台查询用户更名日志（位于用户管理菜单中的日志列表选项卡）和通知日志（位于数据列表菜单中的通知列表选项卡），保证更名通知已经成功下发到各应用后再进行应用升级操作（对于 UCenter 对接其他非 Discuz! X 系统的站点，请确认应用正确实现了 UCenter 通知改名接口。），并在升级完成后通过站点公告、电子邮件、手机短信等多种方式手动通知此类用户通过更名卡等方式自行修改用户名。如果需要通过更名卡方式修改用户名的站点，请在转换完成后手动在后台配置道具中心的更名卡，以支持上述用户自助改名。</p><p class="lead">由于 UCenter 1.7.0 更新了数据库编码，为了防止本地化编码版本内以本地化编码的安全提问内的非 ASCII 文字导致用户登录受阻，因此如您站点为本地化编码则将为您清空安全提问，请将此情况如实告知用户，并要求用户在登录时请不要输入安全提问。如您希望自行编写兼容代码，请自行在后续流程选择不清空用户的安全提问。</p><p class="lead">由于 UCenter 1.7.0 更新了数据库编码，对于极个别采用非标准字符集用作密码的账号可能会出现密码错误的情况，遇到此情况请引导用户选择找回密码并按提示操作找回密码，重新设置密码后即可重新登录。</p><p class="lead">由于 Discuz! X3.5 更新了 IP 黑名单存储方式，因此对于系统内当前批量封禁的 IP 地址，只支持 IPv4 地址规则，且地址掩码为 8 的整数倍且不大于 32 的存量数据，数据库内原有数据将自动删除。建议您在升级前导出原规则，如有需要请在升级后按新系统规则重新配置。</p><p class="lead">由于 Discuz! X3.5 对 Session 、 IP 黑名单功能进行了优化，此类优化由于依赖部分高级特性，因此不支持除 Redis 外的其它内存缓存库。因此如果您的站点流量较大，建议您卸载原有内存缓存库，更换内存缓存库为 Redis ，这样可以最大程度的优化您站点的运行速度，并且不依赖数据库内的 HEAP 表。</p><p class="lead">由于 Discuz! X3.5 对站点功能开关功能进行了优化，升级完成后将为您打开站点所有功能，请在升级完成后自行评估您站点运营具体需要开启的功能，并根据站点运营需求关闭不需要的功能。</p><p class="lead">由于 Discuz! X3.5 更新了数据库编码和默认模板，升级程序会将程序数据表和插件数据表转换至 utf8mb4 ，并试图对您的插件文件进行转码，请您将整个站点所有文件设置为可读写权限，同时在升级完成后将关闭所有非系统插件并恢复默认风格。请您在升级完成后对您的插件进行验证，对于多数插件、模板而言，在转换之后可在 utf8mb4 环境下继续运行或进行少量改造后继续运行，少量插件、模板可能需要自行对文件做转码，甚至进行一定程度的改造才能够正常在 Discuz! X3.5 上运行。</p><p class="lead">请不要重复执行本程序，重复执行可能导致未知的问题，如遇升级出错请不要关闭页面，尝试根据提示解决后刷新继续，如无法实现请恢复备份重新开始升级。</p><p class="lead">在您准备好进行操作之后，点击“下一步”按钮继续操作。</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=license\';">下一步 ></button> <button type="button" class="btn btn-secondary" onclick="location.href=\'?step=welcome\';">上一步 <</button></p>', '请您阅读升级提示', 1);

} else if ($step == 'license') {

	show_msg('<p class="lead">请点击 <a href="https://gitee.com/Discuz/DiscuzX/raw/master/readme/license.txt" target="_blank">https://gitee.com/Discuz/DiscuzX/raw/master/readme/license.txt</a> 链接查看最新版最终用户授权协议。</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=envcheck\';">同意并下一步 ></button> <button type="button" class="btn btn-secondary" onclick="location.href=\'?step=tips\';">上一步 <</button></p>', '请您阅读最终用户授权协议', 1);

} else if ($step == 'envcheck') {

	include_once('../config/config_ucenter.php');
	include_once('../source/discuz_version.php');
	include_once('../uc_client/client.php');

	$tips = '<table class="table table-striped" style="margin: 2em 0;"><thead><tr><th scope="col">软件名称</th><th scope="col">最低要求</th><th scope="col">当前状态</th><th scope="col">是否满足</th></tr></thead><tbody>';
	$env_ok = true;
	$now_ver = array('Code Version' => preg_replace('/[^0-9.]+/', '', constant('DISCUZ_VERSION')), 'UCenter' => uc_check_version()['db'], 'PHP' => constant('PHP_VERSION'), 'MySQL' => helper_dbtool::dbversion(), 'GD' => (function_exists('gd_info') ? preg_replace('/[^0-9.]+/', '', gd_info()['GD Version']) : false), 'XML' => function_exists('xml_parser_create'), 'JSON' => function_exists('json_encode'), 'mbstring' => (function_exists('mb_convert_encoding') || strtoupper(constant('CHARSET')) == 'UTF-8'), 'Not Slave' => $_config['db']['slave'] == false, 'Not DB Map' => empty($_config['db']['map']));// 对于UTF-8用户，不强制要求mbstring扩展
	$req_ver = array('Code Version' => '3.5', 'UCenter' => '1.7.0', 'PHP' => '5.6.0', 'MySQL' => '5.5.3', 'GD' => '1.0', 'XML' => true, 'JSON' => true, 'mbstring' => true, 'Not Slave' => true, 'Not DB Map' => true);
	$lang_ver = array('Code Version' => 'Discuz! 代码版本', 'UCenter' => 'UCenter 版本', 'PHP' => 'PHP 版本', 'MySQL' => 'MySQL 版本', 'GD' => 'GD 扩展', 'XML' => 'XML 扩展', 'JSON' => 'JSON 扩展', 'mbstring' => 'mbstring 扩展 / UTF-8', 'Not Slave' => '未开启从服务器', 'Not DB Map' => '未数据库分布部署');
	foreach ($now_ver as $key => $value) {
		$tips .= "<tr><th>$lang_ver[$key]</th><td>$req_ver[$key]</td><td>$value</td>";
		logmessage("Check $key : $value");
		if ($req_ver[$key] === true) {
			if (!$value) {
				logmessage("Check $key Result: false");
				$tips .= '<td><font color="Red">❌ 不满足</font></td>';
				$env_ok = false;
			} else {
				logmessage("Check $key Result: true");
				$tips .= '<td><font color="Green">✔️ 满足</font></td>';
			}
		} else if (version_compare($value, $req_ver[$key], '<')) {
			logmessage("Check $key Result: false");
			$tips .= '<td><font color="Red">❌ 不满足</font></td>';
			$env_ok = false;
		} else {
			logmessage("Check $key Result: true");
			$tips .= '<td><font color="Green">✔️ 满足</font></td>';
		}
		$tips .= '</tr>';
	}
	$tips .= '</tbody></table><p>';
	$env_ok && $tips .= '<button type="button" class="btn btn-primary" onclick="location.href=\'?step=confirm\';">下一步 ></button> ';
	$tips .= '<button type="button" class="btn btn-secondary" onclick="location.href=\'?step=license\';">上一步 <</button></p>';
	show_msg($tips, '环境检测', 1);

} else if ($step == 'confirm') {

	$myisam_opt = NO_INNODB_FEATURE ? '<button type="button" class="btn btn-secondary" onclick="location.href=\'?step=config&myisam=1\';">不升级到InnoDB（不推荐） > </button>' : '';

	show_msg('<p class="lead" style="color: red;font-weight: bold;">本程序即将开始将您的 Discuz! X3.4 升级至 Discuz! X3.5 ，请确认现在已经做好站点全量数据（含数据库、文件）的备份操作！当您点击“开始升级”时，升级将不可逆的开始！</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=config\';">开始升级 ></button>  ' . $myisam_opt . ' <button type="button" class="btn btn-secondary" onclick="location.href=\'?step=license\';">上一步 <</button></p>', '确认开始', 1);

} else if ($step == 'config') {

	logmessage("upgrade start.");

	if (!C::t('common_setting')->fetch('bbclosed')) {
		logmessage("Ultrax is not close, Upgrader Close it");
		C::t('common_setting')->update('bbclosed', 1);
		require_once libfile('function/cache');
		updatecache('setting');
		show_msg('您的站点未关闭，正在关闭，请稍候......', '提示信息', 0, $theurl.'?step=config');
	}

	$stepurl = $theurl . empty($_GET['myisam']) ? '?step=innodb' : '?step=scheme&myisam=1';

	$deletevar = array('app', 'home'); // config中需要删除的项目
	$default_config = $_config = array();
	$default_configfile = DISCUZ_ROOT.'./config/config_global_default.php';

	if (!file_exists($default_configfile)) {
		logmessage("config_global_default.php not found, not continue.");
		show_msg('<font color="red"><b>config_global_default.php 不存在，无法继续操作，请补充此文件后刷新页面再试。</b></font>', '提示信息');
	} else {
		include $default_configfile;
		$default_config = $_config;
	}

	$configfile = DISCUZ_ROOT.'./config/config_global.php';
	include $configfile;

	logmessage("unlink " . str_replace(DISCUZ_ROOT, '', $tablescachefile) . ".");
	@unlink($tablescachefile);

	// 对非 Windows 系统尝试设置 777 权限
	if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
		logmessage("set chmod 777 $configfile");
		chmod($configfile, 0777);
	}

	if (save_config_file($configfile, $_config, $default_config, $deletevar)) {
		logmessage("config_global_default.php modify ok, continue.");
		show_msg("系统配置设置成功，即将进行下一步操作，请稍候......", '提示信息', 0, "$stepurl");
	} else {
		logmessage("config_global_default.php not write, let user manually move, not continue.");
		show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。由于 "config/" 目录不可写入，我们已将更新的文件保存到 "data/" 目录下，请通过 FTP 软件将其转移到 "config/" 目录下覆盖源文件。随后 <a href="'.$stepurl.'">点击此处</a> 继续操作。</b></font>', '提示信息');
	}

} else if ($step == 'innodb') {

	@touch(DISCUZ_ROOT.'./data/install.lock');
	@unlink(DISCUZ_ROOT.'./install/index.php');

	if ($table) {
		$sql_check = get_convert_sql('check', $table);
		if (!empty($sql_check)) {
			$result = DB::fetch_first($sql_check);
			// 对文字排序进行过滤，避免不合法文字排序进入升级流程。考虑到部分站长自行进行了 utf8mb4 改造，因此额外添加 utf8mb4_general_ci 。
			// 从 MySQL 8.0.28 开始, utf8_general_ci 更名为 utf8mb3_general_ci
			if (!in_array($result['Collation'], array('utf8mb4_unicode_ci', 'utf8_general_ci', 'utf8mb3_general_ci', 'gbk_chinese_ci', 'big5_chinese_ci', 'utf8mb4_general_ci'))) {
				logmessage("table ".$table." 's ci ".$result['Collation']." not support, not continue.");
				show_msg("<font color=\"red\"><b>表 ".$table." 的文字排序 ".$result['Collation']." 不受支持，请人工处理后再继续！</b></font>", '提示信息');
			}
			if (empty($_GET['scheme']) && get_innodb_scheme_update_sql($table, true)) {
				// 对于因数据库超时而升级失败的特大站点请看此函数
				setdbglobal();
				// Q008 好像有些站点存在 gpmid 索引, 这里尝试删除一下
				// https://www.dismall.com/thread-14718-1-1.html
				if ($table == 'pre_common_member_grouppm') {
					logmessage("pre_common_member_grouppm has gpmid index, DROP it first.");
					$sql = "ALTER TABLE pre_common_member_grouppm DROP INDEX gpmid;";
					logmessage("RUNSQL ".$sql);
					DB::query($sql, 'SILENT');
					logmessage("RUNSQL SILENT Success");
				}
				// 帖子分表特殊处理
				// https://www.dismall.com/thread-15265-1-1.html
				if (preg_match("/^pre_forum_post_(\\d+)$/i", $table)) {
					logmessage("$table is special post table, need special alter.");
					$sql = "ALTER TABLE ".str_replace(' pre_', ' '.$config['tablepre'], $table)." MODIFY COLUMN position INT unsigned NOT NULL DEFAULT \'0\'";
					logmessage("RUNSQL ".$sql);
					DB::query($sql);
					logmessage("RUNSQL Success");
				} else {
					$sql = get_innodb_scheme_update_sql($table);
					logmessage("RUNSQL ".$sql);
					DB::query($sql);
					logmessage("RUNSQL Success");
				}
				show_msg("InnoDB数据表升级进行中，$table 结构预调整完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&table=".$table."&scheme=1");
			}
			if ($result['Engine'] != 'InnoDB') {
				$sql = get_convert_sql($step, $table);
				if (!empty($sql)) {
					// 对于因数据库超时而升级失败的特大站点请看此函数
					setdbglobal();
					logmessage("RUNSQL ".$sql);
					DB::query($sql);
					logmessage("RUNSQL Success");
				}
			}
		}
	}

	$tmp = array_flip($tables);
	$tmpid = $tmp[$table];
	$count = count($tables);
	if ($tmpid + 1 < $count) {
		$next = $tables[++$tmpid];
		show_msg("InnoDB数据表升级进行中，$table 表升级已完成，当前进度 $tmpid / $count ，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&table=".$next);
	} else {
		show_msg("InnoDB数据表升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=scheme");
	}

} else if ($step == 'scheme') {

	@touch(DISCUZ_ROOT.'./data/install.lock');
	@unlink(DISCUZ_ROOT.'./install/index.php');

	// 结构升级走原有 X3.4 以前的流程, 避免站点小规模自定义数据结构或版本间数据库结构不匹配的影响
	$sqlfile = DISCUZ_ROOT.'./install/data/install.sql';

	if(!file_exists($sqlfile)) {
		logmessage("install.sql not found, not continue");
		show_msg('<font color="red"><b>SQL文件 '.$sqlfile.' 不存在！</b></font>', '提示信息');
	}

	$sql = implode('', file($sqlfile));
	preg_match_all("/CREATE\s+TABLE.+?pre\_(.+?)\s*\((.+?)\)\s*(ENGINE|TYPE)\s*=\s*(\w+)/is", $sql, $matches);
	$newtables = empty($matches[1])?array():$matches[1];
	$newsqls = empty($matches[0])?array():$matches[0];
	if(empty($newtables) || empty($newsqls)) {
		logmessage("install.sql is empty, not continue");
		show_msg('<font color="red"><b>SQL 文件内容为空，请确认！</b></font>', '提示信息');
	}

	$i = empty($_GET['i'])?0:intval($_GET['i']);
	$count_i = count($newtables);
	if($i>=$count_i) {
		logmessage("install.sql is ok, continue");
		show_msg("数据库结构升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=utf8mb4");
	}
	$newtable = $newtables[$i];

	$specid = intval($_GET['specid']);
	if($specid && in_array($newtable, array('forum_post', 'forum_thread'))) {
		$spectable = $newtable;
		$newtable = get_special_table_by_num($newtable, $specid);
	}

	$newcols = getcolumn($newsqls[$i]);

	// 对于因数据库超时而升级失败的特大站点请看此函数
	setdbglobal();

	if(!$query = DB::query("SHOW CREATE TABLE ".DB::table($newtable), 'SILENT')) {
		preg_match("/(CREATE TABLE .+?)\s*(ENGINE|TYPE)\s*=\s*(\w+)/is", $newsqls[$i], $maths);

		$maths[3] = strtoupper($maths[3]);
		$engine = empty($_GET['myisam']) ? 'InnoDB' : $maths[3];
		$usql = $maths[1].' ENGINE='.$engine.' CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;';

		$usql = str_replace("CREATE TABLE IF NOT EXISTS pre_", 'CREATE TABLE IF NOT EXISTS '.$config['tablepre'], $usql);
		$usql = str_replace("CREATE TABLE pre_", 'CREATE TABLE '.$config['tablepre'], $usql);

		logmessage("RUNSQL: ".$usql);

		if(!DB::query($usql, 'SILENT')) {
			logmessage("RUNSQL FAILED");
			show_msg('<font color="red"><b>添加表 '.DB::table($newtable).' 出错,请手工执行以下 SQL 语句后,再重新运行本升级程序:</b></font><br><br>'.dhtmlspecialchars($usql), '提示信息');
		} else {
			logmessage("RUNSQL Success");
			$msg = '添加表 '.DB::table($newtable).' 完成';
		}
	} else {
		$value = DB::fetch($query);
		logmessage("TABLE: ".implode(",", $value));
		$oldcols = getcolumn($value['Create Table']);

		$updates = array();
		$allfileds =array_keys($newcols);
		foreach ($newcols as $key => $value) {
			if($key == 'PRIMARY') {
				// 如果在五个白名单表内，则不需要做任何事，反之沿用原有流程
				$nomovetable = array('common_cache', 'common_card', 'common_member_profile_setting', 'common_setting', 'mobile_setting');
				if($value != $oldcols[$key]) {
					if(!empty($oldcols[$key]) && !in_array($newtable, $nomovetable)) {
						// 不再默认对数据表进行更名操作, 如果遇到主键不一致的优先报错处理
						if(NO_RENAME_TABLE) {
							logmessage("Table ".$newtable." PRIMARY KEY Define not same as install.sql, not continue.");
							show_msg("<font color=\"red\"><b>表 ".$newtable." 的主键与系统定义不符，系统不支持自动升级，请人工处理后再继续！</b></font>", '提示信息');
						}
						logmessage("RUNSQL: ".$usql);
						$usql = "RENAME TABLE ".DB::table($newtable)." TO ".DB::table($newtable.'_bak');
						if(!DB::query($usql, 'SILENT')) {
							logmessage("Table ".$newtable." Update Failed.");
							show_msg('<font color="red"><b>升级表 '.DB::table($newtable).' 出错,请手工执行以下升级语句后,再重新运行本升级程序:</b></font><br><br><b>升级SQL语句</b>:<div style="position:absolute;font-size:11px;font-family:verdana,arial;background:#EBEBEB;padding:0.5em;">'.dhtmlspecialchars($usql)."</div><br><b>Error</b>: ".DB::error()."<br><b>Errno.</b>: ".DB::errno(), '提示信息');
						} else {
							logmessage("RUNSQL: Success");
							$msg = '表改名 '.DB::table($newtable).' 完成！';
							show_msg($msg, '提示信息', 0, $theurl.'?step=scheme&i='.$_GET['i']);
						}
					}
					if(!in_array($newtable, $nomovetable)) {
						$updates[] = "ADD PRIMARY KEY $value";
					}
				}
			} elseif ($key == 'KEY') {
				foreach ($value as $subkey => $subvalue) {
					if(!empty($oldcols['KEY'][$subkey])) {
						if($subvalue != $oldcols['KEY'][$subkey]) {
							$updates[] = "DROP INDEX `$subkey`";
							$updates[] = "ADD INDEX `$subkey` $subvalue";
						}
					} else {
						$updates[] = "ADD INDEX `$subkey` $subvalue";
					}
				}
			} elseif ($key == 'UNIQUE') {
				foreach ($value as $subkey => $subvalue) {
					if(!empty($oldcols['UNIQUE'][$subkey])) {
						if($subvalue != $oldcols['UNIQUE'][$subkey]) {
							$updates[] = "DROP INDEX `$subkey`";
							$updates[] = "ADD UNIQUE INDEX `$subkey` $subvalue";
						}
					} else {
						$usql = "ALTER TABLE  ".DB::table($newtable)." DROP INDEX `$subkey`";
						DB::query($usql, 'SILENT');
						$updates[] = "ADD UNIQUE INDEX `$subkey` $subvalue";
					}
				}
			} else {
				if(!empty($oldcols[$key])) {
					if(strtolower($value) != strtolower($oldcols[$key])) {
						$updates[] = "CHANGE `$key` `$key` $value";
					}
				} else {
					$i = array_search($key, $allfileds);
					$fieldposition = $i > 0 ? 'AFTER `'.$allfileds[$i-1].'`' : 'FIRST';
					$updates[] = "ADD `$key` $value $fieldposition";
				}
			}
		}

		if(!empty($updates)) {
			$usql = "ALTER TABLE ".DB::table($newtable)." ".implode(', ', $updates);
			logmessage("RUNSQL: ".$usql);
			if(!DB::query($usql, 'SILENT')) {
				logmessage("RUNSQL: FAILED");
				show_msg('<font color="red"><b>升级表 '.DB::table($newtable).' 出错,请手工执行以下升级语句后,再重新运行本升级程序:</b></font><br><br><b>升级SQL语句</b>:<div style="position:absolute;font-size:11px;font-family:verdana,arial;background:#EBEBEB;padding:0.5em;">'.dhtmlspecialchars($usql)."</div><br><b>Error</b>: ".DB::error()."<br><b>Errno.</b>: ".DB::errno(), '提示信息');
			} else {
				logmessage("RUNSQL: Success");
				$msg = '升级表 '.DB::table($newtable).' 完成！';
			}
		} else {
			$msg = '检查表 '.DB::table($newtable).' 完成，不需升级，跳过';
		}
	}

	if($specid) {
		$newtable = $spectable;
	}

	if(get_special_table_by_num($newtable, $specid+1)) {
		$next = $theurl . '?step='.$step.'&i='.($_GET['i']).'&specid='.($specid + 1)."&myisam=".(empty($_GET['myisam']) ? 0 : 1);
	} else {
		$next = $theurl.'?step='.$step.'&i='.($_GET['i']+1)."&myisam=".(empty($_GET['myisam']) ? 0 : 1);
	}
	show_msg("[ $i / $count_i ] ".$msg.'，即将进行下一步操作，请稍候......', '提示信息', 0, $next);

} else if ($step == 'utf8mb4') {

	$sql_check = get_convert_sql('check', $table);
	if (!empty($sql_check)) {
		$result = DB::fetch_first($sql_check);
		if ($result['Collation'] != 'utf8mb4_unicode_ci') {
			// 对文字排序进行过滤，避免不合法文字排序进入升级流程。考虑到部分站长自行进行了 utf8mb4 改造，因此额外添加 utf8mb4_general_ci 。
			// 从 MySQL 8.0.28 开始, utf8_general_ci 更名为 utf8mb3_general_ci
			if (!in_array($result['Collation'], array('utf8mb4_unicode_ci', 'utf8_general_ci', 'utf8mb3_general_ci', 'gbk_chinese_ci', 'big5_chinese_ci', 'utf8mb4_general_ci'))) {
				logmessage("table ".$table." 's ci ".$result['Collation']." not support, not continue.");
				show_msg("<font color=\"red\"><b>表 ".$table." 的文字排序 ".$result['Collation']." 不受支持，请人工处理后再继续！</b></font>", '提示信息');
			}
			$sql = get_convert_sql('utf8mb4', $table);
			if (!empty($sql)) {
				// 对于因数据库超时而升级失败的特大站点请看此函数
				setdbglobal();
				logmessage("RUNSQL ".$sql);
				DB::query($sql);
				logmessage("RUNSQL Success");
			}
		}
	}

	$tmp = array_flip($tables);
	$tmpid = $tmp[$table];
	$count = count($tables);
	if ($tmpid + 1 < $count) {
		$next = $tables[++$tmpid];
		show_msg("utf8mb4数据表升级进行中，$table 表升级已完成，当前进度 $tmpid / $count ，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&table=".$next);
	} else {
		show_msg("utf8mb4数据表升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=serialize");
	}

} else if ($step == 'serialize') {

	if (!isset($_GET['start']) && !isset($_GET['tid'])) {

		logmessage("start serialize convert.");

		if ($config['dbcharset'] == 'utf8' || $config['dbcharset'] == 'utf8mb4') {
			logmessage("serialize not needed because of this site is ".$config['dbcharset']." site.");
			show_msg("序列化数据转换无需进行，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=dataupdate");
		}

		// 针对本地化编码版本转换 UTF-8 版本的情况, 可能前期 InnoDB 或者 UTF8MB4 转换时站长手动删除过不能转换的数据表
		// 因此这里需要删除表缓存, 避免后续序列化转换尤其是第三方序列化转换时尝试升级之前步骤删除的第三方数据表
		$tcfile = DISCUZ_ROOT . './data/sysdata/cache_update_tables.php';
		logmessage("unlink " . str_replace(DISCUZ_ROOT, '', $tcfile) . " for {$config['dbcharset']} version.");
		@unlink($tcfile);

		$configfile = DISCUZ_ROOT.'./config/config_global.php';
		$configfile_uc = DISCUZ_ROOT.'./config/config_ucenter.php';

		// 对非 Windows 系统尝试设置 777 权限
		if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
			logmessage("set chmod 777 $configfile");
			chmod($configfile, 0777);
			logmessage("set chmod 777 $configfile_uc");
			chmod($configfile_uc, 0777);
		}

		if (is_writable($configfile) && is_writable($configfile_uc)) {
			$config = file_get_contents($configfile);
			$config = preg_replace("/\['dbcharset'\] = \s*'.*?'\;/i", "['dbcharset'] = 'utf8mb4';", $config);
			$config = preg_replace("/\['output'\]\['charset'\] = \s*'.*?'\;/i", "['output']['charset'] = 'utf-8';", $config);
			// logmessage("new config_global.php content:");
			// logmessage($config);
			if(file_put_contents($configfile, $config, LOCK_EX) === false) {
				logmessage("config_global.php modify fail, let user manually modify.");
				show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 config/config_global.php 和 config/config_ucenter.php ，将 dbcharset 配置 和 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 charset 配置 和 UC_CHARSET 常量手工改为 utf8，随后 <a href="'.$theurl.'?step=serialize&start=0&tid=0">点击此处</a> 继续操作。</b></font>', '提示信息');
			}
			logmessage("config_global.php modify ok, continue.");
			$config_uc = file_get_contents($configfile_uc);
			$config_uc = preg_replace("/define\('UC_DBCHARSET',\s*'.*?'\);/i", "define('UC_DBCHARSET', 'utf8mb4');", $config_uc);
			$config_uc = preg_replace("/define\('UC_CHARSET',\s*'.*?'\);/i", "define('UC_CHARSET', 'utf-8');", $config_uc);
			// logmessage("new config_ucenter.php content:");
			// logmessage($config_uc);
			if(file_put_contents($configfile_uc, $config_uc, LOCK_EX) === false) {
				logmessage("config_ucenter.php modify fail, let user manually modify.");
				show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 config/config_ucenter.php ，将 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 UC_CHARSET 常量手工改为 utf8，随后 <a href="'.$theurl.'?step=serialize&start=0&tid=0">点击此处</a> 继续操作。</b></font>', '提示信息');
			}
			logmessage("config_ucenter.php modify ok, continue.");
			show_msg("序列化数据转换配置设置成功，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=serialize&fromcharset={$_config['output']['charset']}&start=0&tid=0");
		} else {
			logmessage("config_global.php modify fail, let user manually modify site config and ucenter config.");
			show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 config/config_global.php 和 config/config_ucenter.php ，将 dbcharset 配置 和 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 charset 配置 和 UC_CHARSET 常量手工改为 utf8，随后 <a href="'.$theurl.'?step=serialize&fromcharset='.$_config['output']['charset'].'&start=0&tid=0">点击此处</a> 继续操作。</b></font>', '提示信息');
		}

	}

	$fromcharset = isset($_GET['fromcharset']) && strtolower($_GET['fromcharset']) == 'big5' ? 'big5' : 'gbk';

	// 对于因数据库超时而升级失败的特大站点请看此函数
	setdbglobal();

	$limit = 1000;
	$nextid = 0;

	$start = empty($_GET['start']) ? 0 : intval($_GET['start']);
	$tid = empty($_GET['tid']) ? 0 : intval($_GET['tid']);

	$arr = get_serialize_list();

	$field = $arr[$tid];
	$arr[$tid];
	$stable = str_replace('pre_', $config['tablepre'], $field[0]);
	$sfield = $field[1];
	$sids = explode(',', $field[2]);
	$sid = $sids[0];
	$sid2 = !empty($sids[1]) ? $sids[1] : '';
	$special = $field[3];

	if (in_array($field[0], $tables)) {
		$isblob = false;
		$to_result = DB::fetch_all("SHOW COLUMNS FROM $stable;");
		foreach ($to_result as $tc) {
			if ($sfield == $tc['Field'] && strstr($tc['Type'], 'blob')) {
				$isblob = true;
			}
		}

		if ($special && empty($sid2)) {
			// 空值不参与序列化转换, 加快数据处理效率
			// https://www.dismall.com/thread-15293-1-1.html
			$sql = "SELECT `$sfield`, `$sid` FROM `$stable` WHERE `$sfield`<>'' AND `$sid` > $start ORDER BY `$sid` ASC LIMIT $limit";
		} else {
			$sql = "SELECT `$sfield`, `$sid` " . (!empty($sid2) ? ", `$sid2`" : "") . " FROM `$stable`";
		}

		logmessage("RUNSQL ".$sql);
		$query = DB::query($sql);
		logmessage("RUNSQL Success");

		while ($values = DB::fetch($query)) {
			if ($special) {
				$nextid = $values[$sid];
			} else {
				$nextid = 0;
			}
			$datanew = '';
			$data = $values[$sfield];
			$dataold = $values[$sfield];
			$id = $values[$sid];
			$id2 = !empty($sid2) && !empty($values[$sid2]) ? $values[$sid2] : '';
			if ($isblob) {
				$tmp = dunserialize($data);
				if ($tmp !== false) {
					$datanew = serialize(diconv_array($tmp, $fromcharset, 'UTF-8'));
				} else {
					$data = diconv($data, $fromcharset, 'UTF-8');
				}
			} else {
				// 还原原编码
				$olddata = diconv($data, 'UTF-8', $fromcharset);
				$tmp = dunserialize($olddata);
				if ($tmp !== false) {
					$datanew = serialize(diconv_array($tmp, $fromcharset, 'UTF-8'));
				}
			}
			// 反序列化转码方式无法处理时，才使用正则替换，因为正则匹配在极端情况下匹配可能不准确，比如值里含有";
			if ($datanew === '') {
				$datanew = preg_replace_callback('/s:([0-9]+?):"([\s\S]*?)";/', '_serialize', $data);
			}
			$datanew = addslashes($datanew);
			if (strcmp($dataold, $datanew) !== 0) {
				$sql = "UPDATE `$stable` SET `$sfield` = '$datanew' WHERE `$sid` = '$id'" . (!empty($sid2) ? " AND `$sid2` = '$id2'" : "");
				logmessage("RUNSQL ".$sql);
				DB::query($sql);
				logmessage("RUNSQL Success");
			}
		}
	}

	if ($nextid) {
		show_msg("序列化数据转换进行中，$stable 表升级进行中，即将进行下一步操作（第 $tid 个转换项从 $nextid 开始的数据），请稍候......", '提示信息', 0, "$theurl?step=$step&fromcharset={$fromcharset}&tid=$tid&start=$nextid");
	} else {
		if (++$tid < count($arr)) {
			show_msg("序列化数据转换进行中，$stable 表升级进行中，即将进行下一步操作（第 $tid 个转换项从 $nextid 开始的数据），请稍候......", '提示信息', 0, "$theurl?step=$step&fromcharset={$fromcharset}&tid=$tid&start=0");
		} else {
			show_msg("序列化数据转换完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=serialize_plugin&fromcharset={$fromcharset}");
		}
	}

} else if ($step == 'serialize_plugin') {

	$fromcharset = isset($_GET['fromcharset']) && strtolower($_GET['fromcharset']) == 'big5' ? 'big5' : 'gbk';

	logmessage("start plugin serialize convert.");

	$tables_outside = $arr = array();

	$limit = 1000;
	$nextid = 0;
	$start = empty($_GET['start']) ? 0 : intval($_GET['start']);
	$tid = empty($_GET['tid']) ? 0 : intval($_GET['tid']);
	$cachename = 'update_serialize_plugin';
	$cachefile = DISCUZ_ROOT . './data/sysdata/cache_' . $cachename . '.php';
	if ($tid === 0 || !file_exists($cachefile)) {
		foreach ($tables as $tb) {
			if (in_array(str_replace($config['tablepre'], 'pre_', $tb), $sys_tables_not_full)) {
				continue; // 不对系统内的表做扫描，UCenter表之前已排除过
			}
			if (preg_match('/^'.$config['tablepre'].'forum_(post|thread)_[0-9]+/i', $tb) || strstr($tb, $config['tablepre'].'forum_optionvalue')) {
				continue; // 不对系统内的表做扫描，此处排除主题、帖子分表和分类信息分表
			}
			$tables_outside = array_merge($tables_outside, array($tb));
		}

		// 对于因数据库超时而升级失败的特大站点请看此函数
		setdbglobal();

		foreach ($tables_outside as $to) {
			$fkey = '';
			$to = str_replace('pre_', $config['tablepre'], $to);
			$to_result = DB::fetch_all("SHOW COLUMNS FROM $to;");
			if (!empty($to_result[0]) && strstr($to_result[0]['Type'], 'int') && strstr($to_result[0]['Extra'], 'auto_increment')) {
				$fkey = $to_result[0]['Field'];
			}
			if (empty($fkey)) {
				$keys = array();
				foreach ($to_result as $tc) {
					if ($tc['Key'] === 'PRI') {
						$keys[] = $tc['Field'];
						if (empty($fkey) && strstr($tc['Type'], 'int')) {
							$fkey = $tc['Field'];
						}
					}
				}
				//不处理联合主键的情况
				if (count($keys) > 1) {
					$fkey = '';
				}
			}
			foreach ($to_result as $tc) {
				if (strstr($tc['Type'], 'char') || strstr($tc['Type'], 'text') || strstr($tc['Type'], 'blob')) {
					$tmp_arr = array($to, $tc['Field'], $tc['Type'], $fkey);
					$arr = array_merge($arr, array($tmp_arr));
				}
			}
		}
		logmessage("write " . str_replace(DISCUZ_ROOT, '', $cachefile) . "\t" . count($arr) . ".");
		writetocache($cachename, getcachevars(array('arr' => $arr, 'tables_outside' => $tables_outside)));
	} else {
		@include_once $cachefile;
	}



	$field = $arr[$tid];

	$stable = $field[0];
	$sfield = $field[1];
	$stype = $field[2];
	$skey = $field[3];

	if (!empty($stable) && !empty($sfield)) {
		if (!empty($skey)) {
			// 空值不参与序列化转换, 加快数据处理效率
			// https://www.dismall.com/thread-15293-1-1.html
			$sql = "SELECT `$sfield`, `$skey` FROM `$stable` WHERE `$sfield`<>'' AND `$skey` > $start ORDER BY `$skey` ASC LIMIT $limit";
		} else {
			$sql = "SELECT `$sfield` FROM `$stable`";
		}
		$query = DB::query($sql);

		while ($values = DB::fetch($query)) {
			if (!empty($skey)) {
				$nextid = $values[$skey];
			} else {
				$nextid = 0;
			}
			$datanew = '';
			$data = $values[$sfield];
			if (preg_match('/s:([0-9]+?):"([\s\S]*?)";/', $data) === 1) {
				if (strstr($stype, 'blob')) {
					$tmp = dunserialize($data);
					if ($tmp !== false) {
						$datanew = serialize(diconv_array($tmp, $fromcharset, 'UTF-8'));
					} else {
						$data = diconv($data, $fromcharset, 'UTF-8');
					}
				} else {
					// 还原原编码
					$olddata = diconv($data, 'UTF-8', $fromcharset);
					$tmp = dunserialize($olddata);
					if ($tmp !== false) {
						$datanew = serialize(diconv_array($tmp, $fromcharset, 'UTF-8'));
					}
				}
				// 反序列化转码方式无法处理时，才使用正则替换，因为正则匹配在极端情况下匹配可能不准确，比如值里含有";
				if ($datanew === '') {
					$datanew = preg_replace_callback('/s:([0-9]+?):"([\s\S]*?)";/', '_serialize', $data);
				}
				if (dunserialize($datanew) !== false) {
					$datanew = addslashes($datanew);
					$data = addslashes($data);
					if (strcmp($data, $datanew) !== 0) {
						$sql = "UPDATE `$stable` SET `$sfield` = '$datanew' WHERE `$sfield` = '$data';";
						logmessage("RUNSQL ".$sql);
						DB::query($sql);
						logmessage("RUNSQL Success");
					}
				}
			} elseif (strstr($stype, 'blob')) {
				$datanew = diconv($data, $fromcharset, 'UTF-8');
				$datanew = addslashes($datanew);
				$data = addslashes($data);
				if (strcmp($data, $datanew) !== 0) {
					$sql = "UPDATE `$stable` SET `$sfield` = '$datanew' WHERE `$sfield` = '$data';";
					logmessage("RUNSQL ".$sql);
					DB::query($sql);
					logmessage("RUNSQL Success");
				}
			}
		}
	}

	if ($nextid) {
		show_msg("第三方序列化数据转换进行中，$stable 表升级进行中，即将进行下一步操作（第 $tid 个转换项从 $nextid 开始的数据），请稍候......", '提示信息', 0, "$theurl?step=$step&fromcharset={$fromcharset}&tid=$tid&start=$nextid");
	} else {
		if (++$tid < count($arr)) {
			show_msg("第三方序列化数据转换进行中，$stable 表升级进行中，即将进行下一步操作（第 $tid 个转换项），请稍候......", '提示信息', 0, "$theurl?step=$step&fromcharset={$fromcharset}&tid=$tid&start=0");
		} else {
			logmessage("unlink " . str_replace(DISCUZ_ROOT, '', $cachefile) . ".");
			@unlink($cachefile);
			show_msg("第三方序列化数据转换完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=file");
		}
	}

} else if ($step == 'file') {

	logmessage("start file convert.");

	encode_tree(__DIR__.'/../data/log/');
	encode_tree(__DIR__.'/../source/plugin/');
	encode_tree(__DIR__.'/../template/');

	show_msg("文件编码转换完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=dataupdate");

} else if ($step == 'dataupdate') {

	logmessage("start data update.");
	db_content_update();
	dir_clear(DISCUZ_ROOT.'./data/template');
	dir_clear(DISCUZ_ROOT.'./data/cache');
	dir_clear(DISCUZ_ROOT.'./data/threadcache');
	dir_clear(DISCUZ_ROOT.'./uc_client/data');
	dir_clear(DISCUZ_ROOT.'./uc_client/data/cache');
	//自动删除这个文件，避免站长直接覆盖X3.5文件执行的升级，这个文件会导致缓存更新报错
	@unlink(DISCUZ_ROOT.'./source/function/cache/cache_ipbanned.php');
	
	logmessage("unlink " . str_replace(DISCUZ_ROOT, '', $tablescachefile) . ".");
	@unlink($tablescachefile);

	savecache('setting', '');
	C::memory()->clear();

	$configfile = DISCUZ_ROOT.'./config/config_global.php';
	$configfile_uc = DISCUZ_ROOT.'./config/config_ucenter.php';

	// 对非 Windows 系统尝试设置 777 权限
	if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
		logmessage("set chmod 777 $configfile");
		chmod($configfile, 0777);
		logmessage("set chmod 777 $configfile_uc");
		chmod($configfile_uc, 0777);
	}

	if (is_writable($configfile) && is_writable($configfile_uc)) {
		$config = file_get_contents($configfile);
		$config = preg_replace("/\['dbcharset'\] = \s*'.*?'\;/i", "['dbcharset'] = 'utf8mb4';", $config);
		$config = preg_replace("/\['output'\]\['charset'\] = \s*'.*?'\;/i", "['output']['charset'] = 'utf-8';", $config);
		if(file_put_contents($configfile, $config, LOCK_EX) === false) {
			logmessage("config_global.php modify fail, let user manually modify.");
			show_msg('<font color="red"><b>升级成功，但由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 config/config_global.php 和 config/config_ucenter.php ，将 dbcharset 配置 和 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 charset 配置 和 UC_CHARSET 常量手工改为 utf8。同时参考 UCenter 样例配置文件正确添加 UC_STANDALONE / UC_AVTURL / UC_AVTPATH 三个必要常量。请不要重复执行本程序，重复执行可能导致未知的问题。</b></font><iframe src="../misc.php?mod=initsys" style="display:none;"></iframe>', '提示信息');
		}
		logmessage("config_global.php modify ok, continue.");
		$config_uc = file_get_contents($configfile_uc);
		$config_uc = preg_replace("/define\('UC_DBCHARSET',\s*'.*?'\);/i", "define('UC_DBCHARSET', 'utf8mb4');", $config_uc);
		$config_uc = preg_replace("/define\('UC_CHARSET',\s*'.*?'\);/i", "define('UC_CHARSET', 'utf-8');", $config_uc);
		// 只在数据升级增加应该就行, 序列化升级不依赖这个常量
		$config_uc = str_replace("define('UC_CHARSET', 'utf-8');", "define('UC_CHARSET', 'utf-8');\r\ndefine('UC_STANDALONE', 0);\r\ndefine('UC_AVTURL', '');\r\ndefine('UC_AVTPATH', '');\r\n", $config_uc);
		if(file_put_contents($configfile_uc, $config_uc, LOCK_EX) === false) {
			logmessage("config_ucenter.php modify fail, let user manually modify.");
			show_msg('<font color="red"><b>升级成功，但由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 config/config_ucenter.php ，将 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 UC_CHARSET 常量手工改为 utf8。同时参考 UCenter 样例配置文件正确添加 UC_STANDALONE / UC_AVTURL / UC_AVTPATH 三个必要常量。请不要重复执行本程序，重复执行可能导致未知的问题。</b></font><iframe src="../misc.php?mod=initsys" style="display:none;"></iframe>', '提示信息');
		}
		logmessage("config_ucenter.php modify ok, continue.");
	} else {
		logmessage("config_global.php modify fail, let user manually modify 2 files.");
		show_msg('<font color="red"><b>升级成功，但由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 config/config_global.php 和 config/config_ucenter.php ，将 dbcharset 配置 和 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 charset 配置 和 UC_CHARSET 常量手工改为 utf8。同时参考 UCenter 样例配置文件正确添加 UC_STANDALONE / UC_AVTURL / UC_AVTPATH 三个必要常量。请不要重复执行本程序，重复执行可能导致未知的问题。</b></font><iframe src="../misc.php?mod=initsys" style="display:none;"></iframe>', '提示信息');
	}

	show_msg('数据更新完成，正在更新缓存，请稍候......<iframe src="../misc.php?mod=initsys" style="display:none;" onload="window.location.href=\''.$theurl.'?lock=1\'"></iframe>', '提示信息');

}

function diconv_array($variables, $in_charset, $out_charset) {
	foreach($variables as $_k => $_v) {
		if(is_array($_v)) {
			$variables[$_k] = diconv_array($_v, $in_charset, $out_charset);
		} elseif(is_string($_v)) {
			$variables[$_k] = diconv($_v, $in_charset, $out_charset);
		}
	}
	return $variables;
}

function show_msg($message, $title = '提示信息', $page = 0, $url_forward = '', $time = 1, $noexit = 0, $notice = '') {
	if ($url_forward) {
		$message = "<a href=\"$url_forward\">$message (跳转中......)</a><br />$notice<script>setTimeout(\"window.location.href ='$url_forward';\", $time);</script>";
	}

	if (!$page) {
		$message = '<p class="lead">'.$message.'</p>';
	}

	show_header();
	print<<<END
<main role="main" class="flex-shrink-0">
<div class="container">
<h1 class="mt-5">$title</h1>
$message
<p>$notice</p>
</div>
</main>
END;
	show_footer();
	!$noexit && exit();
}

function show_header() {
	print<<<END
<!DOCTYPE html>
<html class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Discuz! X Community Team">
<title>Discuz! X3.5 升级程序</title>
<link rel="stylesheet" href="?css=1">
<style>
main > .container {padding: 15px 15px 0;}
.footer {background-color: #f5f5f5;}
.footer > .container {padding-right: 15px; padding-left: 15px;}
code {font-size: 80%;}
.lead {margin: 2em 0; line-height: 2em;}
</style>
</head>

<body class="d-flex flex-column h-100">
<header>
<nav class="navbar navbar-expand-md navbar-light bg-light">
<a class="navbar-brand">Discuz! X3.5 升级程序</a>
<div class="collapse navbar-collapse" id="navbarCollapse">
<ul class="navbar-nav mr-auto">
<li class="nav-item">
<a class="nav-link" href="https://gitee.com/Discuz/DiscuzX/" target="_blank">Discuz! X 官方Git</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.discuz.vip/" target="_blank">Discuz! X 官方站</a>
</li>
</ul>
</div>
</nav>
</header>

END;
}

function show_footer() {
	$date = date("Y");
	print<<<END

<footer class="footer mt-auto py-3">
<div class="container">
<span class="text-muted">Powered by Discuz! X Community Team. Copyright &copy; 2001-$date Tencent Cloud.</span>
</div>
</footer>
</body>
</html>
END;
}

function dir_clear($dir) {
	global $lang;
	if ($directory = @dir($dir)) {
		while($entry = $directory->read()) {
			$filename = $dir.'/'.$entry;
			if (is_file($filename)) {
				@unlink($filename);
			}
		}
		$directory->close();
		@touch($dir.'/index.htm');
	}
}

function get_convert_sql($type, $table) {
	global $config;
	$table = str_replace('pre_', $config['tablepre'], $table);
	if ($type == 'innodb') {
		$query = "ALTER TABLE $table ENGINE=InnoDB;";
	} else if ($type == 'utf8mb4') {
		$query = "ALTER TABLE $table CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
	} else if ($type == 'check') {
		$query = "SHOW TABLE STATUS WHERE `Name` = '$table';";
	}
	return $query;
}

function get_innodb_scheme_update_sql($table, $statusonly = false) {
	global $config;
	// 每条数据库处理指令一行
	$query = array(
		'pre_common_admincp_perm' => 'ALTER TABLE pre_common_admincp_perm DROP KEY `cpgroupperm`, ADD KEY `cpgroupperm` (`cpgroupid`, `perm`(40));',
		'pre_common_advertisement_custom' => 'ALTER TABLE pre_common_advertisement_custom DROP KEY `name`, ADD KEY `name` (`name`(100));',
		'pre_common_banned' => 'TRUNCATE TABLE pre_common_banned;',
		'pre_common_block_style' => 'ALTER TABLE pre_common_block_style DROP KEY `hash`, ADD KEY `hash` (`hash`(10)), DROP KEY `blockclass`, ADD KEY `blockclass` (`blockclass`(50));',
		'pre_common_cache' => 'ALTER TABLE pre_common_cache DROP PRIMARY KEY, ADD PRIMARY KEY (`cachekey`(50));',
		'pre_common_card' => 'ALTER TABLE pre_common_card DROP PRIMARY KEY, ADD PRIMARY KEY (`id`(50));',
		'pre_common_member_grouppm' => 'ALTER TABLE pre_common_member_grouppm ADD INDEX gpmid(gpmid);',
		'pre_common_member_profile_setting' => 'ALTER TABLE pre_common_member_profile_setting DROP PRIMARY KEY, ADD PRIMARY KEY (`fieldid`(30));',
		'pre_common_member_secwhite' => 'TRUNCATE TABLE pre_common_member_secwhite;',
		'pre_common_member_security' => 'ALTER TABLE pre_common_member_security DROP KEY `uid`, ADD KEY `uid` (`uid`, `fieldid`(40));',
		// 由于不是每个站点都有用户分表, 因此把他放在预调整能节省开发资源
		'pre_common_member_archive' => 'ALTER TABLE pre_common_member_archive MODIFY COLUMN email varchar(255) NOT NULL DEFAULT \'\', ADD COLUMN `secmobile` varchar(12) NOT NULL DEFAULT \'\' AFTER `password`, ADD COLUMN `secmobicc` varchar(3) NOT NULL DEFAULT \'\' AFTER `password`, ADD COLUMN `secmobilestatus` tinyint(1) NOT NULL DEFAULT \'0\' AFTER avatarstatus, ADD KEY secmobile (`secmobile`, `secmobicc`);',
		'pre_common_member_field_forum_archive' => 'ALTER TABLE pre_common_member_field_forum_archive MODIFY COLUMN authstr varchar(255) NOT NULL DEFAULT \'\', MODIFY COLUMN customshow tinyint(3) unsigned NOT NULL DEFAULT \'26\';',
		'pre_common_member_field_home_archive' => 'ALTER TABLE pre_common_member_field_home_archive ADD COLUMN allowasfollow tinyint(1) NOT NULL DEFAULT \'1\' AFTER addfriend, ADD COLUMN allowasfriend tinyint(1) NOT NULL DEFAULT \'1\' AFTER addfriend;',
		'pre_common_member_profile_archive' => 'ALTER TABLE pre_common_member_profile_archive ADD COLUMN birthcountry varchar(255) NOT NULL DEFAULT \'\' AFTER nationality, ADD COLUMN residecountry varchar(255) NOT NULL DEFAULT \'\' AFTER birthcommunity;',
		'pre_common_member_status_archive' => 'ALTER TABLE pre_common_member_status_archive MODIFY COLUMN regip VARCHAR(45) NOT NULL DEFAULT \'\', MODIFY COLUMN lastip VARCHAR(45) NOT NULL DEFAULT \'\', ADD COLUMN regport SMALLINT(6) unsigned NOT NULL DEFAULT \'0\' AFTER lastip;',
		'pre_common_member_stat_field' => 'ALTER TABLE pre_common_member_stat_field DROP KEY `fieldid`, ADD KEY `fieldid` (`fieldid`(40));',
		'pre_common_process' => 'TRUNCATE TABLE pre_common_process;',
		'pre_common_searchindex' => 'TRUNCATE TABLE pre_common_searchindex;',
		'pre_common_seccheck' => 'TRUNCATE TABLE pre_common_seccheck;',
		'pre_common_setting' => 'ALTER TABLE pre_common_setting DROP PRIMARY KEY, ADD PRIMARY KEY (`skey`(40));',
		'pre_common_session' => 'TRUNCATE TABLE pre_common_session;',
		'pre_common_visit' => 'TRUNCATE TABLE pre_common_visit;',
		'pre_forum_groupfield' => 'ALTER TABLE pre_forum_groupfield DROP KEY `types`, ADD KEY `types` (`fid`, `type`(40)), DROP KEY `type`, ADD KEY `type` (`type`(40));',
		'pre_forum_post' => 'ALTER TABLE pre_forum_post MODIFY COLUMN position INT unsigned NOT NULL DEFAULT \'0\'',
		'pre_forum_rsscache' => 'TRUNCATE TABLE pre_forum_rsscache;',
		'pre_forum_spacecache' => 'TRUNCATE TABLE pre_forum_spacecache;',
		'pre_forum_threaddisablepos' => 'TRUNCATE TABLE pre_forum_threaddisablepos;',
		'pre_home_favorite' => 'ALTER TABLE pre_home_favorite DROP KEY `idtype`, ADD KEY `idtype` (`id`, `idtype`(40)), DROP KEY `uid`, ADD KEY `uid` (`uid`, `idtype`(40), `dateline`);',
		'pre_mobile_setting' => 'ALTER TABLE pre_mobile_setting DROP PRIMARY KEY, ADD PRIMARY KEY (`skey`(40));',
		'pre_portal_topic' => 'ALTER TABLE pre_portal_topic DROP KEY `name`, ADD KEY `name` (`name`(40));'
	);
	return $statusonly ? array_key_exists($table, $query) : str_replace(' pre_', ' '.$config['tablepre'], $query[$table]);
}

function _serialize($str) {
	$l = strlen($str[2]);
	return 's:'.$l.':"'.$str[2].'";';
}

function get_serialize_list() {
	return array(
		array('pre_common_advertisement', 'parameters', 'advid', TRUE),
		array('pre_common_block', 'param', 'bid', TRUE),
		array('pre_common_block', 'blockstyle', 'bid', TRUE),
		array('pre_common_block_item', 'fields', 'itemid', TRUE),
		array('pre_common_block_item_data', 'showstyle', 'dataid', TRUE),
		array('pre_common_block_item_data', 'fields', 'dataid', TRUE),
		array('pre_common_block_style', 'template', 'styleid', TRUE),
		array('pre_common_block_style', 'fields', 'styleid', TRUE),
		array('pre_common_diy_data', 'diycontent', 'targettplname,tpldirectory', FALSE),
		array('pre_common_magic', 'magicperm', 'magicid', TRUE),
		array('pre_common_member_field_forum', 'groups', 'uid', TRUE),
		array('pre_common_member_field_forum', 'groupterms', 'uid', TRUE),
		array('pre_common_member_field_forum_archive', 'groups', 'uid', TRUE),
		array('pre_common_member_field_forum_archive', 'groupterms', 'uid', TRUE),
		array('pre_common_member_field_home', 'blockposition', 'uid', TRUE),
		array('pre_common_member_field_home', 'privacy', 'uid', TRUE),
		array('pre_common_member_field_home', 'acceptemail', 'uid', TRUE),
		array('pre_common_member_field_home', 'magicgift', 'uid', TRUE),
		array('pre_common_member_field_home_archive', 'blockposition', 'uid', TRUE),
		array('pre_common_member_field_home_archive', 'privacy', 'uid', TRUE),
		array('pre_common_member_field_home_archive', 'acceptemail', 'uid', TRUE),
		array('pre_common_member_field_home_archive', 'magicgift', 'uid', TRUE),
		array('pre_common_member_newprompt', 'data', 'uid', TRUE),
		array('pre_common_member_stat_search', 'condition', 'optionid', TRUE),
		array('pre_common_member_verify_info', 'field', 'vid', TRUE),
		array('pre_common_patch', 'rule', 'serial', TRUE),
		array('pre_common_plugin', 'modules', 'pluginid', TRUE),
		array('pre_common_setting', 'svalue', 'skey', FALSE),
		array('pre_common_syscache', 'data', 'cname', FALSE),
		array('pre_forum_activity', 'ufield', 'tid', TRUE),
		array('pre_forum_forumfield', 'creditspolicy', 'fid', TRUE),
		array('pre_forum_forumfield', 'formulaperm' ,'fid', TRUE),
		array('pre_forum_forumfield', 'threadtypes', 'fid', TRUE),
		array('pre_forum_forumfield', 'supe_pushsetting', 'fid', TRUE),
		array('pre_forum_forumfield', 'modrecommend', 'fid', TRUE),
		array('pre_forum_forumfield', 'extra', 'fid', TRUE),
		array('pre_forum_groupfield', 'data', 'fid', TRUE),
		array('pre_forum_grouplevel', 'creditspolicy', 'levelid', TRUE),
		array('pre_forum_grouplevel', 'postpolicy' ,'levelid', TRUE),
		array('pre_forum_grouplevel', 'specialswitch' ,'levelid', TRUE),
		array('pre_forum_medal', 'permission', 'medalid', TRUE),
		array('pre_forum_postcache', 'comment', 'pid', TRUE),
		array('pre_forum_postcache', 'rate', 'pid', TRUE),
		array('pre_forum_spacecache', 'value', 'uid', TRUE),
		array('pre_forum_threadprofile', 'template', 'id', TRUE),
		array('pre_forum_typeoption', 'rules', 'optionid', TRUE),
		array('pre_home_feed', 'title_data', 'feedid', TRUE),
		array('pre_home_feed', 'body_data', 'feedid', TRUE),
		array('pre_home_share', 'body_data', 'sid', TRUE),
		array('pre_mobile_wechat_resource', 'data', 'id', TRUE),
		array('pre_mobile_wsq_threadlist', 'svalue', 'skey', TRUE),
	);
}

function db_content_update() {
	// 对于因数据库超时而升级失败的特大站点请看此函数
	setdbglobal();
	// 开启程序所有功能
	logmessage("open all features.");
	$feats = array('portal', 'forum', 'friend', 'group', 'follow', 'collection', 'guide', 'feed', 'blog', 'doing', 'album', 'share', 'wall', 'homepage', 'ranklist', 'medal', 'task', 'magic', 'favorite');
	foreach ($feats as $type) {
		$funkey = $type.'status';
		$identifier = array('portal' => 1, 'forum' => 2, 'group' => 3, 'feed' => 4, 'ranklist' => 8, 'follow' => 9, 'guide' => 10, 'collection' => 11, 'blog' => 12, 'album' => 13, 'share' => 14, 'doing' => 15, 'friend' => 26, 'favorite' => 27, 'medal' => 29, 'task' => 30, 'magic' => 31);
		$navdata = array('available' => 1);
		$navtype = array(0, 3);
		if (in_array($type, array('blog', 'album', 'share', 'doing', 'follow', 'friend', 'favorite', 'medal', 'task', 'magic'))) {
			$navtype[] = 2;
		}
		C::t('common_nav')->update_by_navtype_type_identifier($navtype, 0, array("$type", "$identifier[$type]"), $navdata);
		C::t('common_setting')->update($funkey, 1);
	}
	// 关闭所有非系统插件
	logmessage("close all plugin without system plugin.");
	DB::query("UPDATE ".DB::table('common_plugin')." SET available='0' WHERE modules NOT LIKE '%s:6:\"system\";i:2;%'");
	// 恢复默认风格
	logmessage("recover default template and style.");
	define('IN_ADMINCP', true);
	require_once libfile('function/admincp');
	require_once libfile('function/importdata');
	$dir = DB::result_first("SELECT t.directory FROM ".DB::table('common_style')." s LEFT JOIN ".DB::table('common_template')." t ON t.templateid=s.templateid WHERE s.styleid='1'");
	import_styles(1, $dir, 1, 0, 0);
	C::t('common_setting')->update('styleid', 1);
	// 标题最小字数设置为 1 , 标题最大字数设置为 80
	// https://gitee.com/Discuz/DiscuzX/issues/I69QWB
	logmessage("update min/maxsubjectsize.");
	C::t('common_setting')->update('minsubjectsize', 1);
	C::t('common_setting')->update('maxsubjectsize', 80);
	// 关闭已经不再支持的前端 MD5 功能
	logmessage("close pwdsafety");
	C::t('common_setting')->update('pwdsafety', 0);
	// 修正微信插件生成的错误电子邮件地址格式
	// 考虑到这种邮箱本来就是无意义的，因此不走高风险的接口同步了，直接每个应用自行完成替换即可
	logmessage("mod null.null email");
	DB::query("UPDATE ".DB::table('common_member')." SET `email` = replace(`email`, 'null.null', 'm.invalid')");
	// 默认开启手机版、优化手机版默认配置
	logmessage("open mobile version and rewrite mobile defualt config");
	DB::query("DELETE FROM ".DB::table('common_setting')." WHERE skey = 'mobile'");
	DB::query("INSERT INTO ".DB::table('common_setting')." VALUES ('mobile','a:13:{s:11:\"allowmobile\";i:1;s:9:\"allowmnew\";i:0;s:13:\"mobileforward\";i:1;s:14:\"mobileregister\";i:1;s:13:\"mobileseccode\";i:0;s:16:\"mobilesimpletype\";i:0;s:15:\"mobilecachetime\";i:0;s:14:\"mobilecomefrom\";s:0:\"\";s:13:\"mobilepreview\";i:0;s:6:\"legacy\";i:1;s:3:\"wml\";i:0;s:6:\"portal\";a:1:{s:6:\"catnav\";i:0;}s:5:\"forum\";a:6:{s:5:\"index\";i:0;s:8:\"statshow\";i:0;s:13:\"displayorder3\";i:1;s:12:\"topicperpage\";i:20;s:11:\"postperpage\";i:10;s:9:\"forumview\";i:0;}}')");
	DB::query("UPDATE ".DB::table('common_nav')." SET url = 'forum.php?showmobile=yes', available = 1 WHERE identifier = 'mobile'");
	// 积分策略清理
	logmessage("common_credit_rule clear");
	DB::query("DELETE FROM ".DB::table('common_credit_rule')." WHERE action IN ('installapp', 'useapp')");
	DB::query("UPDATE ".DB::table('common_credit_rule')." SET extcredits1 = 0, extcredits2 = 0, extcredits3 = 0, extcredits4 = 0, extcredits5 = 0, extcredits6 = 0, extcredits7 = 0, extcredits8 = 0 WHERE action IN ('promotion_visit', 'visit', 'poke')");
	logmessage("wp.qq.com upgrade");
	// QQ Discuz! Code HTTPS 升级
	DB::query("UPDATE ".DB::table('forum_bbcode')." SET replacement = '<a href=\"https://wpa.qq.com/msgrd?v=3&uin={1}&amp;site=[Discuz!]&amp;from=discuz&amp;menu=yes\" target=\"_blank\"><img src=\"static/image/common/qq_big.gif\" border=\"0\"></a>', prompt = '请输入 QQ 号码:<a href=\"\" class=\"xi2\" onclick=\"this.href=\'https://wp.qq.com/set.html?from=discuz&uin=\'+$(\'e_cst1_qq_param_1\').value\" target=\"_blank\" style=\"float:right;\">设置QQ在线状态&nbsp;&nbsp;</a>' WHERE tag = 'qq'");
	// 顶部菜单升级
	logmessage("common_nav upgrade");
	DB::query("UPDATE ".DB::table('common_nav')." SET url = 'home.php?mod=space&do=thread&view=me' WHERE identifier = 'thread'");
	// 用户分表更名
	logmessage("cron_member_optimize_daily upgrade");
	DB::query("UPDATE ".DB::table('common_cron')." SET name = '每日用户分表' WHERE filename = 'cron_member_optimize_daily.php'");
	// 个人信息国别数据升级
	logmessage("common_district upgrade");
	DB::query("INSERT INTO ".DB::table('common_district')." (`name`, `level`, `upid`, `usetype`) VALUES ('中国', 0, 0, 3);");
	$district_upid = DB::insert_id();
	DB::query("UPDATE ".DB::table('common_district')." SET upid = $district_upid, usetype = 0 WHERE level = 1 AND upid = 0");
	DB::query("UPDATE ".DB::table('common_member_profile')." SET birthcountry = '中国' WHERE birthprovince != ''");
	DB::query("UPDATE ".DB::table('common_member_profile')." SET residecountry = '中国' WHERE resideprovince != ''");
	DB::query("INSERT INTO ".DB::table('common_member_profile_setting')." VALUES('birthcountry', 1, 0, 0, '出生国家', '', 0, 0, 0, 0, 0, 0, 0, 'select', 0, '', '')");
	DB::query("INSERT INTO ".DB::table('common_member_profile_setting')." VALUES('residecountry', 1, 0, 0, '居住国家', '', 0, 0, 0, 0, 0, 0, 0, 'select', 0, '', '')");
	// 允许用户浏览个人资料页
	logmessage("common_usergroup_field allowviewprofile=1");
	DB::query("UPDATE ".DB::table('common_usergroup_field')." SET allowviewprofile = '1'");
	// 允许用户上传头像
	// https://www.dismall.com/thread-14793-1-1.html
	logmessage("common_usergroup_field allowavatarupload=1");
	DB::query("UPDATE ".DB::table('common_usergroup_field')." SET allowavatarupload = '1'");
	// 老旧系统插件数据清理
	logmessage("remove old system plugins.");
	$plugins = array('cloudstat', 'soso_smilies', 'security', 'pcmgr_url_safeguard', 'manyou', 'cloudcaptcha', 'cloudunion', 'qqgroup', 'xf_storage', 'cloudsearch', 'qqconnect');
	foreach($plugins as $pluginid) {
		$plugin = C::t('common_plugin')->fetch_by_identifier($pluginid);
		if($plugin) {
			$modules = dunserialize($plugin['modules']);
			$modules['system'] = 0;
			$modules = serialize($modules);
			C::t('common_plugin')->update($plugin['pluginid'], array('modules' => $modules));
		}
	}
}

function save_config_file($filename, $config, $default, $deletevar) {
	$config = setdefault($config, $default, $deletevar);
	$content = <<<EOT
<?php


\$_config = array();

EOT;
	$content .= getvars(array('_config' => $config));
	$content .= "\r\n// ".str_pad('  THE END  ', 50, '-', STR_PAD_BOTH)." //\r\n\r\n?>";
	if (!empty($_GET['myisam'])) {
		$content = preg_replace("/\['db'\]\['common'\]\['engine'\] = \s*'.*?'\;/i", "['db']['common']['engine'] = 'myisam';", $content);
	}
	// logmessage("new config_global_default.php content:");
	// logmessage($content);
	if (!is_writable($filename) || !($len = file_put_contents($filename, $content))) {
		file_put_contents(DISCUZ_ROOT.'./data/config_global.php', $content);
		return 0;
	}
	return 1;
}

function setdefault($var, $default, $deletevar = array()) {
	foreach ($default as $k => $v) {
		if (!isset($var[$k])) {
			$var[$k] = $default[$k];
		} elseif (is_array($v)) {
			$var[$k] = setdefault($var[$k], $default[$k]);
		}
	}
	foreach ($deletevar as $k) {
		unset($var[$k]);
	}
	return $var;
}

function getvars($data, $type = 'VAR') {
	$evaluate = '';
	foreach($data as $key => $val) {
		if (!preg_match("/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/", $key)) {
			continue;
		}
		if (is_array($val)) {
			$evaluate .= buildarray($val, 0, "\${$key}")."\r\n";
		} else {
			$val = addcslashes($val, '\'\\');
			$evaluate .= $type == 'VAR' ? "\$$key = '$val';\n" : "define('".strtoupper($key)."', '$val');\n";
		}
	}
	return $evaluate;
}

function buildarray($array, $level = 0, $pre = '$_config') {
	static $ks;
	if ($level == 0) {
		$ks = array();
		$return = '';
	}

	foreach ($array as $key => $val) {
		if(!preg_match("/^[a-zA-Z0-9_\x7f-\xff]+$/", $key)) {
			continue;
		}

		if ($level == 0) {
			$newline = str_pad('  CONFIG '.strtoupper($key).'  ', 70, '-', STR_PAD_BOTH);
			$return .= "\r\n// $newline //\r\n";
			if ($key == 'admincp') {
				$newline = str_pad(' Founders: $_config[\'admincp\'][\'founder\'] = \'1,2,3\'; ', 70, '-', STR_PAD_BOTH);
				$return .= "// $newline //\r\n";
			}
		}

		$ks[$level] = $ks[$level - 1]."['$key']";
		if (is_array($val)) {
			$ks[$level] = $ks[$level - 1]."['$key']";
			$return .= buildarray($val, $level + 1, $pre);
		} else {
			$val =  is_string($val) || strlen($val) > 12 || !preg_match("/^\-?[1-9]\d*$/", $val) ? '\''.addcslashes($val, '\'\\').'\'' : $val;
			$return .= $pre.$ks[$level - 1]."['$key']"." = $val;\r\n";
		}
	}
	return $return;
}

function encode_file($filename) {
	if (file_exists($filename)) {
		$i = pathinfo($filename);
		if (in_array($i['extension'], array('js', 'css', 'php', 'html', 'htm', 'xml'))) {
			$res = file_get_contents($filename);
			// 有的地方说 EUC-CN = GB2312，CP936 = GBK，这里都写上
			$encode = mb_detect_encoding($res, array("ASCII", "UTF-8", "GB2312", "GBK", "EUC-CN", "CP936", "GB18030", "BIG-5"));
			if (in_array($encode, array("GB2312", "GBK", "EUC-CN", "CP936", "GB18030", "BIG-5"))) {
				$res = mb_convert_encoding($res, "UTF-8", $encode);
				// 对非 Windows 系统尝试设置 777 权限
				if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
					logmessage("set chmod 777 $filename");
					chmod($filename, 0777);
				}
				logmessage("file convert $filename");
				file_put_contents($filename, $res);
			}
		}
	}
}

function encode_tree($directory) {
	$tdir = dir($directory);
	while ($file = $tdir->read()) {
		if ((is_dir("$directory/$file")) && ($file != ".") && ($file != "..")) {
			encode_tree("$directory/$file");
		} else {
			$file ="$directory/$file";
			if (is_file($file)) {
				encode_file($file);
			}
		}
	}
}


function get_special_table_by_num($tablename, $num) {
	$tables_array = get_special_tables_array($tablename);

	$num --;
	return isset($tables_array[$num]) ? $tables_array[$num] : FALSE;
}

function get_special_tables_array($tablename) {
	$tablename = DB::table($tablename);
	$tablename = str_replace('_', '\_', $tablename);
	$query = DB::query("SHOW TABLES LIKE '{$tablename}\_%'");
	$dbo = DB::object();
	$tables_array = array();
	while($row = $dbo->fetch_array($query, MYSQLI_NUM)) {
		if(preg_match("/^{$tablename}_(\\d+)$/i", $row[0])) {
			$prefix_len = strlen($dbo->tablepre);
			$row[0] = substr($row[0], $prefix_len);
			$tables_array[] = $row[0];
		}
	}
	return $tables_array;
}

function getcolumn($creatsql) {

	$creatsql = preg_replace("/ COMMENT '.*?'/i", '', $creatsql);
	preg_match("/\((.+)\)\s*(ENGINE|TYPE)\s*\=/is", $creatsql, $matchs);

	$cols = explode("\n", $matchs[1]);
	$newcols = array();
	foreach ($cols as $value) {
		$value = trim($value);
		if(empty($value)) continue;
		$value = remakesql($value);
		if(substr($value, -1) == ',') $value = substr($value, 0, -1);

		$vs = explode(' ', $value);
		$cname = $vs[0];

		if($cname == 'KEY' || $cname == 'INDEX' || $cname == 'UNIQUE') {

			$name_length = strlen($cname);
			if($cname == 'UNIQUE') $name_length = $name_length + 4;

			$subvalue = trim(substr($value, $name_length));
			$subvs = explode(' ', $subvalue);
			$subcname = $subvs[0];
			$newcols[$cname][$subcname] = trim(substr($value, ($name_length+2+strlen($subcname))));

		}  elseif($cname == 'PRIMARY') {

			$newcols[$cname] = trim(substr($value, 11));

		}  else {

			$newcols[$cname] = trim(substr($value, strlen($cname)));
		}
	}
	return $newcols;
}

function remakesql($value) {
	$value = trim(preg_replace("/\s+/", ' ', $value));
	// 去掉 mediumtext 替换为 text 的功能, 避免应设置字段出现问题
	$value = str_replace(array('`',', ', ' ,', '( ' ,' )'), array('', ',', ',','(',')'), $value);
	return $value;
}

function cssoutput() {
	header('Content-Type: text/css; charset=utf-8');
	$data = base64_decode('H4sIAAAAAAACA61ZCXOrOBL+K6ynUk52EAEc7ATX3vd917G3QI3RBBAl5Ngeyv99JCRjYWzyrpn3DGr1pa9brRbv+24c40wAl88EMsahTdgeNfRbWm3ihHECHEnKMRdl0WasEijDJS0OcYOrBjXAabYuaAUoB7rJRRx4QbRGO0heqUAC9kLpAoTJN9tGzvr+3XkW1yiXQoUSRCkrGI8Fl2przKESx4wx5VgOmMhHiWnlVvitJbSpC3yIk4Klr8eEkUNbYr6hVeyvbQcRrusCUHNoBJTuT6WTr3/A6d+64S8lnzv7G2wYOP/4zcz9K0uYYO7s11C8gaApdv4IW5i5P+EUF+7sj3LS+Zv0bObOfk8T4FhQVhnKGQh39hNl0vmZWorzi5J9Q2dnK2PC3w5lwoqZ0W9L6YUo5OKAQ6mHO43wk+9fIB6tNXhfhUEYhS/rDnYsYa3iAjKxTnD6uuFsWxFkGLMsO+aBwQ0JVkvszEACIVgZe5G0e6ynWALFgQnh0DTteMqsQRwKiCvGS1wM3KZVDpyK47Z418a2cDSXNeEfk9ZGRQJJgB9xa1bo+6skyzQUBFKmQyYdqWCMh511OM7ZG/CznmiZLEZ6pCxwtRzJXzFx/6+cQ/afB/2eFrhp/vNgVJiFXnFlUtp2Y1pHygi4NYfB9vzbL//AKob+Cpttgbn7B6gK5koSTpn7M1Y1TFoZJLNil6n4M7blFLjM/93MLSVN4pKCnY8qLTi8F7S18j4r2C7GW8HWqGzQiWJyokk5K4oE8yMtN62cU/uuMHlbUkIKWOsCpAX0YgVOCmgNPZUKcN1AbF7kdN5a2W/DZkin4lNikebIxDzZSr+rk1aOCd02sW/IccZSOVSx0a/ojTZUevHQsq1QOdCzXi9ExgsLQ0O5uh+Mph4+Y8zQ9eq6fM0YLzUm/5JAwg80w3/adMsbxuOa0UoAP/5LHOp+0jVaDAqqRAKWylKI9cyQXS+a0EaBTh6M9AV10mAco5J9izRutKqAGyXjibbGhKhTx78S9rgPXEYLQNu6YJggsxoFbI/p7aXJaNVb0Z8ftOrg18eIgFISBZxmtVUvD1xTJu3KeFmRo1FFDk+i55iHuqJ6BWBikSXvSN/C97tNbbE9r7zo7lTm4XkBz+l6J1FCO47rOOGAX5EaH/EPleSwbKj9Ojw212PN1gGi+J1OzThlB4q1C511U+KPXipFMFXx3FEicn3km9AirvGJ6n1PKiAzFA2z5tFVQ1M0iyIcf1wCodi5LyVVq49Wy3r/0FpmS7w/zT359f54RWi1fL4ptApvCL28hDeFXpY3hILQ929KBYF20ONs18cnK2C/Vj86tOpniAyywVLQaIpCvmhr1tDuZOBQYEHfYP2RUdBqOvsJbmgT+9qZjfQxDtbGeaPx6OlybNkYnwTD3DIijiDu6S3vd7636vbCxVkgWG0qQnfaBPXeaVhBifMVAQhh2atUnaJSdyGvfTmpMJ6FN7WojvLr7re1rN7iR43gtAai5RzB40rkiGVIVcF7RmR9HLUbfJPge99V/3t+9HD0ElFdLUujvu9mn5eCqr3r66fotlHFFApIxfsNkMHJgtme7CO16ELlmIgN29VRezo8WE3FU2pNsnZOOF4QNQ7gBhCtENsK13bzOodWfHt2j5ocE7YbzfVbteaQAW8QB7JNgaCSdR7p4UPbhab31DRcXcDs/syOyLg/U9y6czg3DOuzb7HvqP+9UOFpUiMIF24YRa4XmvToj9uW1Til4hB7y0jPDE/jbujdPJyVBKo5LTE/tOfLwCgj+gbaQrgnDrRYOEzoWr6Ql5GuZZimQ10apU/VNQnq4tkNngyqClTLbI/el0FkOiIxTlVdnjal1zMyFaXJJ5gyqH4sOA2krCLv5skyXUUrYrvaE209H5gpEV6Gy+cLbdFTlCzDS216VZ+qbTJXgoXcgh0mIzzsdPliwHx6wowWZRt7giiKEtvYl0uZMUanO5jW2ORs93DRTVf4bbLNsZuR0aVStdiN+Z5w1ofUZ52L3tao0Y26ow6kM6temNuPTU6O67bRn2B+paeaWkR38iIqoGxOp7L69EWzA1I9oKTE3ZUaJSB2ANVVd7Vlx+oav6TJk36UcFyRUedhg9j1Pt4i0Ee2IZqQnOl2f6o7gNEF59pdd73LqQDU+SYhV2sZutZHy6a9HzF0PdEI5ZCe+o1tWX1Kvhntzjn3hk21f6G0FzrtDqu11g2z3V2PA9nLC7bZFNYl2Qvt/msa7uDTG76rrdulU3aYLOp0oGxWRFN2owM2Vwsvkmsyy9GD663uKd9nM2vNceTfPSqwne6nYohDDfjcBFq3v+XKe9GXQ+Mi7GssYSvJD63dOB30iSvnSKvOh+5LD2c7R++C0fbtmBqBuTiONDh21g8TXamcEJhIZF2N7HUZ0nESl3Nh0guZMN5vCHujfo+WNeMCy8Sz9om6+k9oMkk0PGx6/kItxxlWvPE17OVhSsAk+BSHTvb3Fd8AfywYfZCg7dk0500PV7cMGVyv+tZ3GqO5YFqd2etWQaIl3kC85cX9jGCB42782Lxtvt6XhXu3SOWrI1+r5gfzXIg6fnzc7XbebuExvnkMfd9XzHOn22c/mC/8uaMLhX5/o7D7Kdv/YK76l4X6M79bgFRbY5E7jeDsFX4wVyu4C5991zF/vOgufJmbeQUgpLiWfMrlnlxSAbygJZW2Ar8nG0/CuUN+MP/Dk7PKw1A+gkg/w4V8zh+1F8p3+TZTTeZGI9aO27vsOXvJ8Hl3HHHPbTLAInSBNp9ZL9gsqsV7xSDB4ENkGfR0wFv9GH+EsVkJUpt3vLHNtBojfQS3Vw/mEXOTc5nByG+tUezbbDmSpb3Vce/OVXuyFCiy/71iwaG05+sDWrR2wxNMMVifti6MqDpl21HjAQu3WXShHTF152W5FUDawZ3B4vkO2wSYX7UdAAA=');
	if(empty($_SERVER['HTTP_ACCEPT_ENCODING']) || strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === false) {
		echo gzdecode($data);
	} else {
		header('Content-Encoding: gzip');
		echo $data;
	}
	exit;
}

function logmessage($message, $close = false) {
	static $fh = false;
	if(!$fh) {
		$fh = fopen(UPGRADE_LOG_PATH, "a+");
	} 
	if($fh) {
		fwrite($fh, $message."\r\n");
		fflush($fh);
		if($close) {
			fclose($fh);
		}
	}
}

function setdbglobal() {
	// 对于因数据库超时而升级失败的特大站点，请赋予升级程序所使用的 MySQL 账户 SUPER privilege 权限，并解除以下三行代码注释后再试
	// DB::query('SET GLOBAL connect_timeout=28800');
	// DB::query('SET GLOBAL wait_timeout=28800');
	// DB::query('SET GLOBAL interactive_timeout=28800');
}

function timezone_set($timeoffset = 8) {
	if(function_exists('date_default_timezone_set')) {
		@date_default_timezone_set('Etc/GMT'.($timeoffset > 0 ? '-' : '+').(abs($timeoffset)));
	}
}