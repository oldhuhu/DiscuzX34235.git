<?php

@set_time_limit(0);
@ignore_user_abort(TRUE);
ini_set('max_execution_time', 0);
ini_set('mysql.connect_timeout', 0);

include_once('../source/class/class_core.php');
include_once('../source/function/function_core.php');

$cachelist = array();
$discuz = C::app();

$discuz->cachelist = $cachelist;
$discuz->init_cron = false;
$discuz->init_setting = true;
$discuz->init_user = false;
$discuz->init_session = false;
$discuz->init_misc = false;

$discuz->init();

// 出生国家/居住国家不会被老版本升级程序导入
// https://gitee.com/oldhuhu/DiscuzX34235/pulls/25
if(!DB::result_first("SELECT * FROM ".DB::table('common_member_profile_setting')." WHERE fieldid='birthcountry'")) {
	DB::query("INSERT INTO ".DB::table('common_member_profile_setting')." VALUES('birthcountry', 1, 0, 0, '出生国家', '', 0, 0, 0, 0, 0, 0, 0, 'select', 0, '', '')");
	DB::query("INSERT INTO ".DB::table('common_member_profile_setting')." VALUES('residecountry', 1, 0, 0, '居住国家', '', 0, 0, 0, 0, 0, 0, 0, 'select', 0, '', '')");
}

exit("升级完毕, 请您登录后台更新缓存即可. 本次异常对您造成的不便我们深感歉意, 并感谢您一直以来对我们的理解与支持!");