<?php

@set_time_limit(0);
@ignore_user_abort(TRUE);
ini_set('max_execution_time', 0);
ini_set('mysql.connect_timeout', 0);

include_once('../source/class/class_core.php');
include_once('../source/function/function_core.php');

$cachelist = array();
$discuz = C::app();

$discuz->cachelist = $cachelist;
$discuz->init_cron = false;
$discuz->init_setting = true;
$discuz->init_user = false;
$discuz->init_session = false;
$discuz->init_misc = false;

$discuz->init();

// Alpha 1-2
if(!DB::result_first("SELECT * FROM ".DB::table('common_district')." WHERE level='0'")) {
	// 新增字段
	DB::query('ALTER TABLE '.DB::table('common_member_profile').' ADD COLUMN birthcountry varchar(255) NOT NULL DEFAULT \'\' AFTER nationality, ADD COLUMN residecountry varchar(255) NOT NULL DEFAULT \'\' AFTER birthcommunity;');
	DB::query('ALTER TABLE '.DB::table('common_member_profile_history').' ADD COLUMN birthcountry varchar(255) NOT NULL DEFAULT \'\' AFTER nationality, ADD COLUMN residecountry varchar(255) NOT NULL DEFAULT \'\' AFTER birthcommunity;');

	// 个人信息国别数据升级
	DB::query("INSERT INTO ".DB::table('common_district')." (`name`, `level`, `upid`, `usetype`) VALUES ('中国', 0, 0, 3);");
	$district_upid = DB::insert_id();
	DB::query("UPDATE ".DB::table('common_district')." SET upid = $district_upid, usetype = 0 WHERE level = 1 AND upid = 0");
	DB::query("UPDATE ".DB::table('common_member_profile')." SET birthcountry = '中国' WHERE birthprovince != ''");
	DB::query("UPDATE ".DB::table('common_member_profile')." SET residecountry = '中国' WHERE resideprovince != ''");
}

//修复 特殊情况下由于text类型默认值的问题导致异常报错的现象 https://gitee.com/Discuz/DiscuzX/pulls/1732
DB::query('ALTER TABLE '.DB::table('common_smsgw').' CHANGE `sendrule` `sendrule` text NOT NULL, CHANGE `parameters` `parameters` text NOT NULL;');
DB::query('ALTER TABLE '.DB::table('common_smslog').' CHANGE `content` `content` text NOT NULL;');
DB::query('ALTER TABLE '.DB::table('common_smslog_archive').' CHANGE `content` `content` text NOT NULL;');

exit("升级完毕, 请您登录后台更新缓存即可继续测试, 请不要重复执行本程序, 感谢您的支持!");