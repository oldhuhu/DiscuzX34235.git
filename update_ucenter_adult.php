<?php

// 不升级到 InnoDB 开关
// 仅限极少量特殊场合应用
define("NO_INNODB_FEATURE", false);
define('UPGRADE_LOG_PATH', __DIR__.'/../data/logs/X3.5_upgrade_ucenter.php');
empty($_GET['css']) || cssoutput();

@set_time_limit(0);
@ignore_user_abort(TRUE);
ini_set('max_execution_time', 0);
ini_set('mysql.connect_timeout', 0);

define("IN_UC", TRUE);
define('UC_ROOT', realpath('..').'/');

require UC_ROOT.'./data/config.inc.php';
require UC_ROOT.'./lib/dbi.class.php';
require UC_ROOT.'./release/release.php';

header("Content-type: text/html; charset=utf-8");

$step = !empty($_GET['step']) && in_array($_GET['step'], array('welcome', 'tips', 'license', 'envcheck', 'confirm', 'secques', 'innodb', 'utf8mb4_other', 'scheme', 'utf8mb4_user', 'serialize', 'file', 'dataupdate', 'sendnote')) ? $_GET['step'] : 'welcome';
$theurl = htmlspecialchars($_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME']);
$lockfile = UC_ROOT.'./data/upgrade.lock';

timezone_set();

$tables = array('uc_admins', 'uc_applications', 'uc_badwords', 'uc_domains', 'uc_failedlogins', 'uc_feeds', 'uc_friends', 'uc_mailqueue', 'uc_memberfields', 'uc_members', 'uc_mergemembers', 'uc_newpm', 'uc_notelist', 'uc_pm_indexes', 'uc_pm_lists', 'uc_pm_members', 'uc_pm_messages_0', 'uc_pm_messages_1', 'uc_pm_messages_2', 'uc_pm_messages_3', 'uc_pm_messages_4', 'uc_pm_messages_5', 'uc_pm_messages_6', 'uc_pm_messages_7', 'uc_pm_messages_8', 'uc_pm_messages_9', 'uc_protectedmembers', 'uc_settings', 'uc_sqlcache', 'uc_tags', 'uc_vars');

$table = empty($_GET['table']) ? 'uc_admins' : (in_array($_GET['table'], $tables) ? $_GET['table'] : 'uc_admins');

// PHP 8 拦截
if (version_compare(PHP_VERSION, '8.0.0', '>=')) {
	show_msg('升级程序支持的 PHP 版本为 5.6 - 7.4 ，请先降级 PHP 至 7.4 再升级 UCenter 程序和其他应用，程序和全部应用均升级完成后可以使用 PHP 8.0 或更高版本。');
}

// Q004 保证无写入权限时可以正常报错
// https://www.dismall.com/thread-14718-1-1.html
if (!@touch(UPGRADE_LOG_PATH) || @!is_writable(UPGRADE_LOG_PATH)) {
	show_msg('请您先登录服务器，给 UCenter 目录下的 data/logs 目录赋予写权限，随后再次运行本文件进行升级。');
}

logmessage("<?php exit;?>\t".date("Y-m-d H:i:s"));
logmessage("Query String: ".$_SERVER['QUERY_STRING']);

$scheme_count = 0;

if (!empty($_GET['lock'])) {
	@touch($lockfile);
	@unlink($theurl);
	logmessage("upgrade success.");
	show_msg('恭喜，您已经成功升级到 UCenter 1.7.0 版本，感谢您的使用！');
}

if (file_exists($lockfile)) {
	logmessage("upgrade locked.");
	show_msg('请您先登录服务器，手工删除 data/upgrade.lock 文件，再次运行本文件进行升级。');
}

if ($step == 'welcome') {

	show_msg('<p class="lead">UCenter 是腾讯云旗下 Discuz! X 系列产品之间信息直接传递的一个桥梁，通过 UCenter 站长可以无缝整合腾讯云旗下 Discuz! X 系列产品，实现用户的一站式登录以及社区其他数据的交互。UCenter拥有机制完善的接口，经过简单修改便可以挂接其它任何平台的第三方的网络应用程序，随时为您的社区系统增加能量。</p><p class="lead">UCenter 1.7.0 版本在继承和完善先前产品宗旨的基础上， Discuz! 社区以“不忘初心”为主线，针对“系统安全”、“IPv6”、“用户体验”和“管理体验”几大方面，对程序进行了全面优化和打造，多项功能改进。本程序将不定期的在官方 Git 中更新，欲追求更新版本的站长欢迎关注。</p><p class="lead">本程序将引导您从 UCenter 1.6.0 升级至 UCenter 1.7.0 ，请在升级之前做好站点全量数据（含数据库、文件）的备份操作，并小心操作。</p><p class="lead">在您准备好进行操作之后，点击“下一步”按钮继续操作。</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=tips\';">下一步 ></button></p>', '欢迎您执行 UCenter 1.7.0 升级程序', 1);

} else if ($step == 'tips') {

	show_msg('<p class="lead">本程序将引导您从 UCenter 1.6.0 升级至 UCenter 1.7.0 ，请在升级之前做好站点全量数据（含数据库、文件）的备份操作，并小心操作。同时建议既有站点升级前将 MySQL 升级至 5.7 及以上版本，以避免插件建表索引过长导致升级中断。</p><p class="lead">由于 UCenter 1.7.0 更新了数据库编码，对于您当前数据库内不支持 utf8mb4_unicode_ci 编码的用户名，本程序会将用户重命名为 15 位随机字符，请您自行在升级成功后在 UCenter 后台查询用户更名日志（位于用户管理菜单中的日志列表选项卡）和通知日志（位于数据列表菜单中的通知列表选项卡），保证更名通知已经成功下发到各应用后再进行应用升级操作（对于 UCenter 对接其他非 Discuz! X 系统的站点，请确认应用正确实现了 UCenter 通知改名接口。），并在升级完成后通过站点公告、电子邮件、手机短信等多种方式手动通知此类用户通过更名卡等方式自行修改用户名。如果需要通过更名卡方式修改用户名的站点，请在转换完成后手动在后台配置道具中心的更名卡，以支持上述用户自助改名。</p><p class="lead">由于 UCenter 1.7.0 更新了数据库编码，为了防止本地化编码版本内以本地化编码的安全提问内的非 ASCII 文字导致用户登录受阻，因此如您站点为本地化编码则将为您清空安全提问，请将此情况如实告知用户，并要求用户在登录时请不要输入安全提问。如您希望自行编写兼容代码，请自行在后续流程选择不清空用户的安全提问。</p><p class="lead">由于 UCenter 1.7.0 更新了数据库编码，对于极个别采用非标准字符集用作密码的账号可能会出现密码错误的情况，遇到此情况请引导用户选择找回密码并按提示操作找回密码，重新设置密码后即可重新登录。</p><p class="lead">请不要重复执行本程序，重复执行可能导致未知的问题，如遇升级出错请不要关闭页面，尝试根据提示解决后刷新继续，如无法实现请恢复备份重新开始升级。</p><p class="lead">在您准备好进行操作之后，点击“下一步”按钮继续操作。</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=license\';">下一步 ></button> <button type="button" class="btn btn-secondary" onclick="location.href=\'?step=welcome\';">上一步 <</button></p>', '请您阅读升级提示', 1);

} else if ($step == 'license') {

	show_msg('<p class="lead">请点击 <a href="https://gitee.com/Discuz/DiscuzX/raw/master/readme/license.txt" target="_blank">https://gitee.com/Discuz/DiscuzX/raw/master/readme/license.txt</a> 链接查看最新版最终用户授权协议。</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=envcheck\';">同意并下一步 ></button> <button type="button" class="btn btn-secondary" onclick="location.href=\'?step=tips\';">上一步 <</button></p>', '请您阅读最终用户授权协议', 1);

} else if ($step == 'envcheck') {

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	$tips = '<table class="table table-striped" style="margin: 2em 0;"><thead><tr><th scope="col">软件名称</th><th scope="col">最低要求</th><th scope="col">当前状态</th><th scope="col">是否满足</th></tr></thead><tbody>';
	$env_ok = true;
	$uc_db_ver = $db->fetch_first("SELECT * FROM ".UC_DBTABLEPRE."settings WHERE `k` = 'version'");
	$now_ver = array('Code Version' => constant('UC_SERVER_VERSION'), 'DB Version' => $uc_db_ver['v'], 'PHP' => constant('PHP_VERSION'), 'MySQL' => $db->version(), 'GD' => (function_exists('gd_info') ? preg_replace('/[^0-9.]+/', '', gd_info()['GD Version']) : false), 'XML' => function_exists('xml_parser_create'), 'JSON' => function_exists('json_encode'), 'mbstring' => (function_exists('mb_convert_encoding') || strtoupper(constant('UC_CHARSET')) == 'UTF-8'));// 对于UTF-8用户，不强制要求mbstring扩展
	$req_ver = array('Code Version' => '1.7.0', 'DB Version' => '1.6.0', 'PHP' => '5.6.0', 'MySQL' => '5.5.3', 'GD' => '1.0', 'XML' => true, 'JSON' => true, 'mbstring' => true);
	$lang_ver = array('Code Version' => 'UCenter 代码版本', 'DB Version' => 'UCenter 数据版本', 'PHP' => 'PHP 版本', 'MySQL' => 'MySQL 版本', 'GD' => 'GD 扩展', 'XML' => 'XML 扩展', 'JSON' => 'JSON 扩展', 'mbstring' => 'mbstring 扩展 / UTF-8');
	foreach ($now_ver as $key => $value) {
		$tips .= "<tr><th>$lang_ver[$key]</th><td>$req_ver[$key]</td><td>$value</td>";
		logmessage("Check $key : $value");
		if ($req_ver[$key] === true) {
			if (!$value) {
				logmessage("Check $key Result: false");
				$tips .= '<td><font color="Red">❌ 不满足</font></td>';
				$env_ok = false;
			} else {
				logmessage("Check $key Result: true");
				$tips .= '<td><font color="Green">✔️ 满足</font></td>';
			}
		} else if (version_compare($value, $req_ver[$key], '<')) {
			logmessage("Check $key Result: false");
			$tips .= '<td><font color="Red">❌ 不满足</font></td>';
			$env_ok = false;
		} else {
			logmessage("Check $key Result: true");
			$tips .= '<td><font color="Green">✔️ 满足</font></td>';
		}
		$tips .= '</tr>';
	}
	$tips .= '</tbody></table><p>';
	$env_ok && $tips .= '<button type="button" class="btn btn-primary" onclick="location.href=\'?step=confirm\';">下一步 ></button> ';
	$tips .= '<button type="button" class="btn btn-secondary" onclick="location.href=\'?step=license\';">上一步 <</button></p>';
	show_msg($tips, '环境检测', 1);

} else if ($step == 'confirm') {

	$myisam_opt = NO_INNODB_FEATURE ? '<button type="button" class="btn btn-secondary" onclick="location.href=\'?step=secques&myisam=1\';">不升级到InnoDB（不推荐） > </button>' : '';

	$myisam_opt .= in_array(strtolower(UC_DBCHARSET), array('utf8', 'utf8mb4')) ? '' : ' <button type="button" class="btn btn-secondary" onclick="location.href=\'?step=scheme&myisam=1\';">不升级到InnoDB且不清空安全提问（不推荐） > </button>';

	$secques_opt = in_array(strtolower(UC_DBCHARSET), array('utf8', 'utf8mb4')) ? '' : '<button type="button" class="btn btn-secondary" onclick="location.href=\'?step=innodb\';">不清空安全提问（不推荐） > </button>';

	show_msg('<p class="lead" style="color: red;font-weight: bold;">本程序即将开始将您的 UCenter 1.6.0 升级至 UCenter 1.7.0 ，请确认现在已经做好站点全量数据（含数据库、文件）的备份操作！当您点击“开始升级”时，升级将不可逆的开始！</p><p class="lead" style="color: red;font-weight: bold;">请务必在升级之前检查 UCenter 到各个应用的通信是否正常，并且请确认对接其他非 Discuz! X 系统的应用正确实现了 UCenter 通知改名接口，如果上述两项未实现，请自行调试通信和实现通知改名接口，否则可能导致 UCenter 升级无法正确完成。</p><p><button type="button" class="btn btn-primary" onclick="location.href=\'?step=secques\';">开始升级 ></button> ' . $secques_opt .' ' . $myisam_opt . ' <button type="button" class="btn btn-secondary" onclick="location.href=\'?step=license\';">上一步 <</button></p>', '确认开始', 1);

} else if ($step == 'secques') {

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	// 对于因数据库超时而升级失败的特大站点请看此函数
	setdbglobal($db);

	$secques_result = "UTF-8 编码版本不需要清空安全提问，即将进行下一步操作，请稍候......";

	if(!in_array(strtolower(UC_DBCHARSET), array('utf8', 'utf8mb4'))) {
		logmessage("clear secques");
		$db->query(str_replace(' uc_', ' '.UC_DBTABLEPRE, 'UPDATE uc_members SET `secques`=\'\';'));
		$secques_result = "安全提问清空完成，即将进行下一步操作，请稍候......";
	}

	$type = empty($_GET['myisam']) ? 'InnoDB' : 'MyISAM';
	$next = $type == 'InnoDB' ? 'innodb' : 'scheme&myisam=1';

	show_msg($secques_result, '提示信息', 0, "$theurl?step=".$next);

} else if ($step == 'innodb') {

	@touch(UC_ROOT.'./data/install.lock');
	@unlink(UC_ROOT.'./install/index.php');

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	if ($table) {
		$sql_check = get_convert_sql('check', $table);
		if (!empty($sql_check)) {
			$result = $db->fetch_first($sql_check);
			// 对文字排序进行过滤，避免不合法文字排序进入升级流程。考虑到部分站长自行进行了 utf8mb4 改造，因此额外添加 utf8mb4_general_ci 。
			// 从 MySQL 8.0.28 开始, utf8_general_ci 更名为 utf8mb3_general_ci
			if (!in_array($result['Collation'], array('utf8mb4_unicode_ci', 'utf8_general_ci', 'utf8mb3_general_ci', 'gbk_chinese_ci', 'big5_chinese_ci', 'utf8mb4_general_ci'))) {
				logmessage("table ".$table." 's ci ".$result['Collation']." not support, not continue.");
				show_msg("<font color=\"red\"><b>表 ".$table." 的文字排序 ".$result['Collation']." 不受支持，请人工处理后再继续！</b></font>", '提示信息');
			}
			if ($table == 'uc_badwords') {
				// 对于因数据库超时而升级失败的特大站点请看此函数
				setdbglobal($db);
				$sql = str_replace(' uc_', ' '.UC_DBTABLEPRE, 'ALTER TABLE uc_badwords DROP KEY `find`, ADD KEY `find` (`find`(100));');
				logmessage("RUNSQL: $sql");
				$db->query($sql);
				logmessage("RUNSQL Success");
			}
			if ($result['Engine'] != 'InnoDB') {
				$sql = get_convert_sql($step, $table);
				if (!empty($sql)) {
					// 对于因数据库超时而升级失败的特大站点请看此函数
					setdbglobal($db);
					logmessage("RUNSQL: $sql");
					$db->query($sql);
					logmessage("RUNSQL Success");
				}
			}
		}
	}

	$tmp = array_flip($tables);
	$tmpid = $tmp[$table];
	$count = count($tables);
	if ($tmpid + 1 < $count) {
		$next = $tables[++$tmpid];
		show_msg("InnoDB数据表升级进行中，$table 表升级已完成，当前进度 $tmpid / $count ，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&table=".$next);
	} else {
		show_msg("InnoDB数据表升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=scheme");
	}

} else if ($step == 'scheme') {

	@touch(UC_ROOT.'./data/install.lock');
	@unlink(UC_ROOT.'./install/index.php');

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	$id = empty($_GET['id']) ? 0 : intval($_GET['id']);
	$type = empty($_GET['myisam']) ? 'InnoDB' : 'MyISAM';

	$sql = get_scheme_update_sql($id, $type);
	// Q004 好像有些站点不存在 email 索引, 这里尝试判断一下, 遇到了就不删除直接跳过
	// https://www.dismall.com/thread-14718-1-1.html
	if ($sql == 'ALTER TABLE uc_members DROP KEY `email`;') {
		$found = false;
		$db_result_index = $db->fetch_all("SHOW INDEX FROM ".UC_DBTABLEPRE."members;");
		foreach ($db_result_index as $dbi) {
			if ($dbi['Key_name'] == 'email') {
				$found = true;
				break;
			}
		}
		if(!$found) {
			show_msg("数据库结构升级进行中，当前进度 $id / $scheme_count ，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&myisam=".(empty($_GET['myisam']) ? 0 : 1)."&id=".++$id);
		}
	}
	if (!empty($sql)) {
		// 对于因数据库超时而升级失败的特大站点请看此函数
		setdbglobal($db);
		logmessage("RUNSQL: $sql");
		$db->query($sql);
		logmessage("RUNSQL Success");
		show_msg("数据库结构升级进行中，当前进度 $id / $scheme_count ，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&myisam=".(empty($_GET['myisam']) ? 0 : 1)."&id=".++$id);
	} else {
		show_msg("数据库结构升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=utf8mb4_user");
	}

} else if ($step == 'utf8mb4_user') {

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	$sql_check = get_convert_sql('check', 'uc_members');
	if (!empty($sql_check)) {
		$result = $db->fetch_first($sql_check);
		if ($result['Collation'] != 'utf8mb4_unicode_ci') {
			// 对文字排序进行过滤，避免不合法文字排序进入升级流程。考虑到部分站长自行进行了 utf8mb4 改造，因此额外添加 utf8mb4_general_ci 。
			// 从 MySQL 8.0.28 开始, utf8_general_ci 更名为 utf8mb3_general_ci
			if (!in_array($result['Collation'], array('utf8mb4_unicode_ci', 'utf8_general_ci', 'utf8mb3_general_ci', 'gbk_chinese_ci', 'big5_chinese_ci', 'utf8mb4_general_ci'))) {
				logmessage("table uc_members 's ci ".$result['Collation']." not support, not continue.");
				show_msg("<font color=\"red\"><b>表 uc_members 的文字排序 ".$result['Collation']." 不受支持，请人工处理后再继续！</b></font>", '提示信息');
			}
			$sql = get_convert_sql('utf8mb4', 'uc_members');
			if (!empty($sql)) {
				// 对于因数据库超时而升级失败的特大站点请看此函数
				setdbglobal($db);
				logmessage("RUNSQL: $sql");
				$db->query($sql, 'SILENT');
			}
		} else {
			logmessage("RUNSQL Success");
			show_msg("utf8mb4用户表升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=utf8mb4_other");
		}
	}

	if ($db->errno() == 1062 && !isset($_GET['fast'])) {// 1062属于编码的问题，进行转换编码
		// 使用宅魂提供的 SQL 语句批量处理用户重名问题
		$sql = "SELECT group_concat(uid) AS uids, group_concat(username), count(1) AS count FROM ".UC_DBTABLEPRE."members GROUP BY CONVERT(username USING utf8mb4) COLLATE utf8mb4_unicode_ci HAVING count > 1;";
		logmessage("RUNSQL: $sql");
		$result = $db->fetch_all($sql);
		foreach ($result as $key => $value) {
			$arr = explode(',', $value['uids']);
			// 对 UID 排序, 最小的 UID 不参与随机命名
			if (is_array($arr) && sort($arr)) {
				array_shift($arr);
			}
			foreach ($arr as $uid) {
				if(!chgusername($uid, true)) {
					logmessage("CHGUSERNAME UID $uid Failed.");
					show_msg("<font color=\"red\"><b>UID ".$uid." 随机命名失败，请人工处理后再继续！</b></font>", '提示信息');
				} else {
					logmessage("CHGUSERNAME UID $uid Success.");
				}
			}
		}
		show_msg("部分用户名不支持utf8mb4编码，已进行随机命名，utf8mb4用户表升级进行中，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&fast=true");
	} elseif ($db->errno() == 1062 && isset($_GET['fast'])) {
		preg_match('/Duplicate entry \'(.*)\' for key/', $db->error(), $matches);
		$username = $matches[1];
		if (chgusername(addslashes($username))) {
			$fast = isset($_GET['fast']) ? '&fast=true' : '';
			logmessage("CHGUSERNAME USERNAME $username Success.");
			show_msg($username." 用户名不支持utf8mb4编码，已进行随机命名，utf8mb4用户表升级进行中，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step.$fast);
		} else {
			logmessage("CHGUSERNAME USERNAME $username Failed.");
			show_msg("<font color=\"red\"><b>".$username." 用户名不支持utf8mb4编码，随机命名失败，请人工处理后再继续！</b></font>", '提示信息');
		}
	} elseif ($db->errno() != 0) {
		// 对于因数据库超时而升级失败的特大站点请看此函数
		setdbglobal($db);
		logmessage("RUNSQL: $sql");
		$db->query($sql);// 如果程序接不住，那就不接了，直接报错
		logmessage("RUNSQL Success");
	} else {
		show_msg("utf8mb4用户表升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=utf8mb4_other");
	}

} else if ($step == 'utf8mb4_other') {

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	$sql_check = get_convert_sql('check', $table);
	if (!empty($sql_check)) {
		$result = $db->fetch_first($sql_check);
		if ($result['Collation'] != 'utf8mb4_unicode_ci') {
			// 对文字排序进行过滤，避免不合法文字排序进入升级流程。考虑到部分站长自行进行了 utf8mb4 改造，因此额外添加 utf8mb4_general_ci 。
			// 从 MySQL 8.0.28 开始, utf8_general_ci 更名为 utf8mb3_general_ci
			if (!in_array($result['Collation'], array('utf8mb4_unicode_ci', 'utf8_general_ci', 'utf8mb3_general_ci', 'gbk_chinese_ci', 'big5_chinese_ci', 'utf8mb4_general_ci'))) {
				logmessage("table ".$table." 's ci ".$result['Collation']." not support, not continue.");
				show_msg("<font color=\"red\"><b>表 ".$table." 的文字排序 ".$result['Collation']." 不受支持，请人工处理后再继续！</b></font>", '提示信息');
			}
			$sql = get_convert_sql('utf8mb4', $table);
			if (!empty($sql) && $table != 'uc_members') {
				// 对于因数据库超时而升级失败的特大站点请看此函数
				setdbglobal($db);
				logmessage("RUNSQL: $sql");
				$db->query($sql);
				logmessage("RUNSQL Success");
			}
		}
	}

	$tmp = array_flip($tables);
	$tmpid = $tmp[$table];
	$count = count($tables);
	if ($tmpid + 1 < $count) {
		$next = $tables[++$tmpid];
		show_msg("utf8mb4数据表升级进行中，$table 表升级已完成，当前进度 $tmpid / $count ，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=".$step."&table=".$next);
	} else {
		show_msg("utf8mb4数据表升级完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=serialize");
	}

} else if ($step == 'serialize') {

	if (!isset($_GET['start']) && !isset($_GET['tid'])) {

		if (constant('UC_DBCHARSET') == 'utf8' || constant('UC_DBCHARSET') == 'utf8mb4') {
			show_msg("序列化数据转换无需进行，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=dataupdate");
		}
	
		$configfile = UC_ROOT.'./data/config.inc.php';
		// 对非 Windows 系统尝试设置 777 权限
		if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
			logmessage("set chmod 777 $configfile");
			chmod($configfile, 0777);
		}
		if (is_writable($configfile)) {
			$config = file_get_contents($configfile);
			$config = preg_replace("/define\('UC_DBCHARSET',\s*'.*?'\);/i", "define('UC_DBCHARSET', 'utf8mb4');", $config);
			$config = preg_replace("/define\('UC_CHARSET',\s*'.*?'\);/i", "define('UC_CHARSET', 'utf-8');", $config);
			if(file_put_contents($configfile, $config, LOCK_EX) === false) {
				logmessage("config.inc.php modify fail, let user manually modify.");
				show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 data/config.inc.php ，将 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 UC_CHARSET 常量手工改为 utf8，随后 <a href="'.$theurl.'?step=serialize&start=0&tid=0">点击此处</a> 继续操作。</b></font>', '提示信息');
			}
			logmessage("config.inc.php modify ok, continue.");
			show_msg("序列化数据转换配置设置成功，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=serialize&start=0&tid=0");
		} else {
			logmessage("config.inc.php modify fail, let user manually modify.");
			show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 data/config.inc.php ，将 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 UC_CHARSET 常量手工改为 utf8，随后 <a href="'.$theurl.'?step=serialize&start=0&tid=0">点击此处</a> 继续操作。</b></font>', '提示信息');
		}

	}

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	// 对于因数据库超时而升级失败的特大站点请看此函数
	setdbglobal($db);

	$limit = 100000;
	$nextid = 0;

	$start = empty($_GET['start']) ? 0 : intval($_GET['start']);
	$tid = empty($_GET['tid']) ? 0 : intval($_GET['tid']);

	$arr = get_serialize_list();

	$field = $arr[$tid];
	$stable = str_replace('uc_', constant('UC_DBTABLEPRE'), $field[0]);
	$sfield = $field[1];
	$sid = $field[2];
	$special = $field[3];

	if ($special) {
		// 空值不参与序列化转换, 加快数据处理效率
		// https://www.dismall.com/thread-15293-1-1.html
		$sql = "SELECT `$sfield`, `$sid` FROM `$stable` WHERE `$sfield`<>'' AND `$sid` > $start ORDER BY `$sid` ASC LIMIT $limit";
	} else {
		$sql = "SELECT `$sfield`, `$sid` FROM `$stable`";
	}

	$query = $db->query($sql);

	$dum = '';

	while ($values = $db->fetch_array($query)) {
		if ($special) {
			$nextid = $values[$sid];
		} else {
			$nextid = 0;
		}
		$data = $values[$sfield];
		$dataold = $values[$sfield];
		$id = $values[$sid];
		$data = preg_replace_callback('/s:([0-9]+?):"([\s\S]*?)";/', '_serialize', $data);
		$data = addslashes($data);
		if (strcmp($dataold, $datanew) !== 0) {
			$sql = "UPDATE `$stable` SET `$sfield` = '$data' WHERE `$sid` = '$id';";
			logmessage("RUNSQL: $sql");
			$db->query($sql);
			logmessage("RUNSQL Success");
		}
	}

	if ($nextid) {
		show_msg("序列化数据转换进行中，$stable 表升级进行中，即将进行下一步操作（第 $tid 个转换项从 $nextid 开始的数据），请稍候......", '提示信息', 0, "$theurl?step=$step&tid=$tid&start=$nextid");
	} else {
		if (++$tid < count($arr)) {
			show_msg("序列化数据转换进行中，$stable 表升级进行中，即将进行下一步操作（第 $tid 个转换项从 $nextid 开始的数据），请稍候......", '提示信息', 0, "$theurl?step=$step&tid=$tid&start=0");
		} else {
			show_msg("序列化数据转换完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=file");
		}
	}

} else if ($step == 'file') {

	logmessage("start file convert.");

	encode_tree(__DIR__.'/../data/logs/');

	show_msg("文件编码转换完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=dataupdate");

} else if ($step == 'dataupdate') {

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	// 对于因数据库超时而升级失败的特大站点请看此函数
	setdbglobal($db);

	logmessage("update version in db.");
	$db->query("REPLACE INTO ".constant('UC_DBTABLEPRE')."settings (k, v) VALUES('version', '1.7.0')");//note 记录数据库版本

	logmessage("clear cache dir.");
	dir_clear(UC_ROOT.'./data/view');
	dir_clear(UC_ROOT.'./data/cache');

	if (is_dir(UC_ROOT.'./plugin/setting')) {
		dir_clear(UC_ROOT.'./plugin/setting');
		@unlink(UC_ROOT.'./plugin/setting/index.htm');
		@rmdir(UC_ROOT.'./plugin/setting');
	}

	logmessage("clear cache dir.");
	$configfile = UC_ROOT.'./data/config.inc.php';
	// 对非 Windows 系统尝试设置 777 权限
	if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
		logmessage("set chmod 777 $configfile");
		chmod($configfile, 0777);
	}
	if (is_writable($configfile)) {
		$config = file_get_contents($configfile);
		$config = preg_replace("/define\('UC_DBCHARSET',\s*'.*?'\);/i", "define('UC_DBCHARSET', 'utf8mb4');", $config);
		$config = preg_replace("/define\('UC_CHARSET',\s*'.*?'\);/i", "define('UC_CHARSET', 'utf-8');", $config);
		// 插入 ONLYREMOTEADDR 设置, 并完善 IPGETTER 对 PHP 5.6 的兼容
		$config = str_replace("define('UC_CHARSET', 'utf-8');", "define('UC_CHARSET', 'utf-8');\r\ndefine('UC_ONLYREMOTEADDR', 1);\r\ndefine('UC_IPGETTER', 'header');\r\n// define('UC_IPGETTER_HEADER', serialize(array('header' => 'HTTP_X_FORWARDED_FOR')));", $config);
		if(file_put_contents($configfile, $config, LOCK_EX) === false) {
			logmessage("config.inc.php modify fail, let user manually modify.");
			show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 data/config.inc.php ，将 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 UC_CHARSET 常量手工改为 utf8，随后 <a href="'.$theurl.'?step=sendnote">点击此处</a> 继续操作。</b></font>', '提示信息');
		}
		logmessage("config.inc.php modify ok, continue.");
	} else {
		logmessage("config.inc.php modify fail, let user manually modify.");
		show_msg('<font color="red"><b>由于配置文件不可写，程序自动修改配置文件失败。需要您手工修改 data/config.inc.php ，将 UC_DBCHARSET 常量手工改为 utf8mb4 ，将 UC_CHARSET 常量手工改为 utf8，随后 <a href="'.$theurl.'?step=sendnote">点击此处</a> 继续操作。</b></font>', '提示信息');
	}

	show_msg("更新缓存完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=sendnote");

} else if ($step == 'sendnote') {

	$db = new ucserver_db();
	$db->connect(UC_DBHOST, UC_DBUSER, UC_DBPW, UC_DBNAME, 'utf8mb4');

	// 对于因数据库超时而升级失败的特大站点请看此函数
	setdbglobal($db);

	$retry = empty($_GET['retry']) ? 0 : (int)$_GET['retry'];

	chgusername_sendallnote($retry);

	show_msg("发送通知完成，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?lock=1");
}

function show_msg($message, $title = '提示信息', $page = 0, $url_forward = '', $time = 1, $noexit = 0, $notice = '') {
	if ($url_forward) {
		$message = "<a href=\"$url_forward\">$message (跳转中......)</a><br />$notice<script>setTimeout(\"window.location.href ='$url_forward';\", $time);</script>";
	}

	if (!$page) {
		$message = '<p class="lead">'.$message.'</p>';
	}

	show_header();
	print<<<END
<main role="main" class="flex-shrink-0">
<div class="container">
<h1 class="mt-5">$title</h1>
$message
<p>$notice</p>
</div>
</main>
END;
	show_footer();
	!$noexit && exit();
}

function show_header() {
	print<<<END
<!DOCTYPE html>
<html class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Discuz! X Community Team">
<title>UCenter 1.7.0 升级程序</title>
<link rel="stylesheet" href="?css=1">
<style>
main > .container {padding: 15px 15px 0;}
.footer {background-color: #f5f5f5;}
.footer > .container {padding-right: 15px; padding-left: 15px;}
code {font-size: 80%;}
.lead {margin: 2em 0; line-height: 2em;}
</style>
</head>

<body class="d-flex flex-column h-100">
<header>
<nav class="navbar navbar-expand-md navbar-light bg-light">
<a class="navbar-brand">UCenter 1.7.0 升级程序</a>
<div class="collapse navbar-collapse" id="navbarCollapse">
<ul class="navbar-nav mr-auto">
<li class="nav-item">
<a class="nav-link" href="https://gitee.com/Discuz/DiscuzX/" target="_blank">Discuz! X 官方Git</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.discuz.vip/" target="_blank">Discuz! X 官方站</a>
</li>
</ul>
</div>
</nav>
</header>

END;
}

function show_footer() {
	$date = date("Y");
	print<<<END

<footer class="footer mt-auto py-3">
<div class="container">
<span class="text-muted">Powered by Discuz! X Community Team. Copyright &copy; 2001-$date Tencent Cloud.</span>
</div>
</footer>
</body>
</html>
END;
}

function chgusername($masterkey, $isuid = false) {
	global $db;
	if ($isuid) {
		$masterkey = intval($masterkey);
		$user = $db->fetch_first("SELECT * FROM ".UC_DBTABLEPRE."members WHERE uid='$masterkey'");
	} else {
		$user = $db->fetch_first("SELECT * FROM ".UC_DBTABLEPRE."members WHERE username='$masterkey'");
	}
	if ($user['uid']) {
		$uid = $user['uid'];
		$username = $user['username'];
		$newusername = get_random_username();
		$extinfo = 'uid='.$uid.'&oldusername='.urlencode($username).'&newusername='.urlencode($newusername);
		$db->query("UPDATE ".UC_DBTABLEPRE."members SET username='$newusername' WHERE uid='$uid'");
		$db->query("INSERT INTO ".UC_DBTABLEPRE."memberlogs SET uid='$uid', action='renameuser', extra='$extinfo'");
		return chgusername_note('renameuser', $extinfo);
	} else {
		return false;
	}
}

function chgusername_note($operation, $getdata = '') {
	global $db;
	$extra = '';
	$apps = $db->fetch_all("SELECT * FROM ".UC_DBTABLEPRE."applications");

	foreach((array)$apps as $appid => $app) {
		$appadd[] = 'app'.$app['appid']."='0'";
	}

	if ($appadd) {
		$extra = implode(',', $appadd);
		$extra = $extra ? ', '.$extra : '';
	}

	$getdata = addslashes($getdata);
	$db->query("INSERT INTO ".UC_DBTABLEPRE."notelist SET getdata='$getdata', operation='$operation', pri='0', postdata=''$extra");

	if ($db->insert_id()) {
		return $db->query("REPLACE INTO ".UC_DBTABLEPRE."vars (name, value) VALUES ('noteexists', '1');");
	} else {
		return false;
	}
}

function get_random_username() {
	global $db;
	$tmp = random(15);
	foreach (range(1, 3) as $try) {
		$user = $db->fetch_first("SELECT * FROM ".UC_DBTABLEPRE."members WHERE username='$tmp'");
		if(isset($user['uid']) && $try < 3) {
			$tmp = random(15);
			continue;
		} else {
			break;
		}
	}
	return $tmp;
}

function random($length, $numeric = 0) {
	if ($numeric) {
		$hash = sprintf('%0'.$length.'d', compromise_random_int(0, pow(10, $length) - 1));
	} else {
		$hash = '';
		$chars = '0123456789abcdef';
		$max = strlen($chars) - 1;
		for($i = 0; $i < $length; $i++) {
			$hash .= $chars[compromise_random_int(0, $max)];
		}
	}
	return $hash;
}

function compromise_random_int($min, $max) {
	if (function_exists('random_int')) {
		try {
			return random_int($min, $max);
		} catch (Exception $e) {
			return mt_rand($min, $max);
		}
	} else {
		return mt_rand($min, $max);
	}
}

function dir_clear($dir) {
	$directory = dir($dir);
	while ($entry = $directory->read()) {
		$filename = $dir.'/'.$entry;
		if (is_file($filename)) {
			@unlink($filename);
		}
	}
	@touch($dir.'/index.htm');
	$directory->close();
}

function get_convert_sql($type, $table) {
	$table = str_replace('uc_', UC_DBTABLEPRE, $table);
	if ($type == 'innodb') {
		$query = "ALTER TABLE $table ENGINE=InnoDB;";
	} else if ($type == 'utf8mb4') {
		$query = "ALTER TABLE $table CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
	} else if ($type == 'check') {
		$query = "SHOW TABLE STATUS WHERE `Name` = '$table';";
	}
	return $query;
}

function get_scheme_update_sql($id, $type = "InnoDB") {
	global $scheme_count;
	// 每条数据库处理指令一行
	// 修正微信插件生成的错误电子邮件地址格式
	// 考虑到这种邮箱本来就是无意义的，因此不走高风险的接口同步了，直接每个应用自行完成替换即可
	$query = array(
		'TRUNCATE TABLE uc_vars;',
		'UPDATE uc_members SET `email` = replace(`email`, \'null.null\', \'m.invalid\');',
		'ALTER TABLE uc_members DROP KEY `email`;',
		'ALTER TABLE uc_members MODIFY COLUMN email varchar(255) NOT NULL DEFAULT \'\', MODIFY COLUMN regip VARCHAR(45) NOT NULL DEFAULT \'\', MODIFY COLUMN `password` varchar(255) NOT NULL DEFAULT \'\', MODIFY COLUMN salt varchar(20) NOT NULL DEFAULT \'\', ADD COLUMN `secmobile` varchar(12) NOT NULL DEFAULT \'\' AFTER `password`, ADD COLUMN `secmobicc` varchar(3) NOT NULL DEFAULT \'\' AFTER `password`, ADD KEY secmobile (`secmobile`, `secmobicc`);',
		'ALTER TABLE uc_members ADD KEY `email` (`email`(40));',
		'CREATE TABLE IF NOT EXISTS uc_memberlogs (lid int(10) unsigned NOT NULL AUTO_INCREMENT, uid mediumint(8) unsigned NOT NULL, action varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'\', extra varchar(255) NOT NULL COLLATE utf8mb4_unicode_ci DEFAULT \'\', PRIMARY KEY(lid)) ENGINE='.$type.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;',
		'ALTER TABLE uc_domains MODIFY COLUMN ip varchar(45) NOT NULL default \'\'',
		'ALTER TABLE uc_failedlogins MODIFY COLUMN ip varchar(45) NOT NULL default \'\', MODIFY COLUMN count tinyint(3) unsigned NOT NULL default \'0\'',
		'ALTER TABLE uc_protectedmembers MODIFY COLUMN appid tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_lists MODIFY COLUMN pmtype tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_0 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_1 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_2 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_3 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_4 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_5 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_6 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_7 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_8 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';',
		'ALTER TABLE uc_pm_messages_9 MODIFY COLUMN delstatus tinyint(3) unsigned NOT NULL default \'0\';'
	);
	$scheme_count = count($query);
	if ($id + 1 > $scheme_count) {
		return '';
	} else {
		return str_replace(' uc_', ' '.UC_DBTABLEPRE, $query[$id]);
	}
}

function _serialize($str) {
        $l = strlen($str[2]);
        return 's:'.$l.':"'.$str[2].'";';
}

function get_serialize_list() {
	return array(
		array('uc_applications', 'extra', 'appid', TRUE),
		array('uc_pm_lists', 'lastmessage', 'plid', TRUE),
		array('uc_settings', 'v', 'k', TRUE),
	);
}

function chgusername_sendallnote($retry) {
	global $db, $theurl;
	$retrytimes = $failedstd = 0;
	$retrytimes = $retry;
	$sql = "SELECT * FROM ".UC_DBTABLEPRE."notelist WHERE closed='0' AND operation='renameuser' ORDER BY pri DESC, noteid ASC";
	logmessage("RUNSQL: $sql");
	$notes = $db->fetch_all($sql);
	logmessage("RUNSQL Success");
	if (!empty($notes)) {
		$apps = $db->fetch_all("SELECT * FROM ".UC_DBTABLEPRE."applications");
		// 失败标准: 当前所有的未发送通知 * 当前所有的应用个数(发给每个应用) * 3(重试系数), 保底重试 30 次
		$failedstd = max((int)count($notes) * (int)count($apps) * 3, 30);
		foreach ((array)$notes as $key => $note) {
			foreach((array)$apps as $appid => $app) {
				$appnotes = $note['app'.$app['appid']];
				if ($app['recvnote'] && $appnotes != 1 && $appnotes > -5) {
					logmessage("SendNote: {$app['appid']} ".implode(" ", $note));
					if (chgusername_sendonenote($app['appid'], 0, $note)) {
						logmessage("SendNote Success");
						// 发成功了, 重试次数清 0
						$retrytimes = 0;
						continue;
					} else {
						if ($retrytimes > $failedstd) {
							logmessage("SendNote Failed");
							show_msg("<font color=\"red\"><b>升级成功，但部分通知发送失败，请您登录 UCenter 用户管理中心，在数据列表菜单中的通知列表选项卡检查更名通知是否全部发出，如发送失败请检查站点与 UCenter 间的通信是否正常，通知全部成功发出后方可继续升级其余应用。请不要重复执行本程序，重复执行可能导致未知的问题。</b></font>", '提示信息');
						} else {
							logmessage("SendNote Failed, But I Can Retry");
							// 当刷新重试的时候, 失败标准次数是不准确的, 因此不显示
							$retrytimes++;
							if ($retrytimes == 1) {
								$failedstd = "未知";
							}
							show_msg("发送通知中，当前通知已重试 $retrytimes 次，当前参考失败标准为 $failedstd 次，即将进行下一步操作，请稍候......", '提示信息', 0, "$theurl?step=sendnote&retry=$retrytimes", '8000');
						}
					}
				}
			}
		}
	}
}

function chgusername_sendonenote($appid, $noteid = 0, $note = '') {
	global $db;
	require_once UC_ROOT.'./lib/xml.class.php';
	$return = FALSE;
	$app = $db->fetch_first("SELECT * FROM ".UC_DBTABLEPRE."applications WHERE appid='$appid'");
	if ($noteid) {
		$note = $db->fetch_first("SELECT * FROM ".UC_DBTABLEPRE."notelist WHERE noteid='$noteid'");
	}

	// 不考虑数据库方式发送，一律走实现更简单的接口方式
	$url = chgusername_get_url_code($note['operation'], $note['getdata'], $appid);
	$note['postdata'] = str_replace(array("\n", "\r"), '', $note['postdata']);
	$response = trim(dfopen2($url, 0, $note['postdata'], '', 1, $app['ip'], 60, TRUE));

	logmessage("SendNote Response: $response");

	$returnsucceed = $response != '' && ($response == 1 || is_array(xml_unserialize($response)));
	$time = time();

	if ($returnsucceed) {
		$db->query("UPDATE ".UC_DBTABLEPRE."notelist SET app$appid='1', totalnum=totalnum+1, succeednum=succeednum+1, dateline='$time' WHERE noteid='$note[noteid]'", 'SILENT');
		$return = TRUE;
	} else {
		$db->query("UPDATE ".UC_DBTABLEPRE."notelist SET app$appid = app$appid-'1', totalnum=totalnum+1, dateline='$time' WHERE noteid='$note[noteid]'", 'SILENT');
		$return = FALSE;
	}
	return $return;
}

function chgusername_get_url_code($operation, $getdata, $appid) {
	global $db;
	$app = $db->fetch_first("SELECT * FROM ".UC_DBTABLEPRE."applications WHERE appid='$appid'");
	$authkey = $app['authkey'];
	$url = $app['url'];
	$apifilename = isset($app['apifilename']) && $app['apifilename'] ? $app['apifilename'] : 'uc.php';
	$action = 'action=renameuser';
	$code = urlencode(authcode("$action&".($getdata ? "$getdata&" : '')."time=".time(), 'ENCODE', $authkey));
	return $url."/api/$apifilename?code=$code";
}

function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {

	$ckey_length = 4;

	$key = md5($key ? $key : UC_KEY);
	$keya = md5(substr($key, 0, 16));
	$keyb = md5(substr($key, 16, 16));
	$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';

	$cryptkey = $keya.md5($keya.$keyc);
	$key_length = strlen($cryptkey);

	$string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
	$string_length = strlen($string);

	$result = '';
	$box = range(0, 255);

	$rndkey = array();
	for($i = 0; $i <= 255; $i++) {
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}

	for($j = $i = 0; $i < 256; $i++) {
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}

	for($a = $j = $i = 0; $i < $string_length; $i++) {
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}

	if ($operation == 'DECODE') {
		if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
			return substr($result, 26);
		} else {
			return '';
		}
	} else {
		return $keyc.str_replace('=', '', base64_encode($result));
	}

}

function dfopen2($url, $limit = 0, $post = '', $cookie = '', $bysocket = FALSE, $ip = '', $timeout = 15, $block = TRUE, $encodetype  = 'URLENCODE', $allowcurl = TRUE) {
	$__times__ = isset($_GET['__times__']) ? intval($_GET['__times__']) + 1 : 1;
	if ($__times__ > 2) {
		return '';
	}
	$url .= (strpos($url, '?') === FALSE ? '?' : '&')."__times__=$__times__";
	return dfopen($url, $limit, $post, $cookie, $bysocket, $ip, $timeout, $block, $encodetype, $allowcurl);
}

function dfopen($url, $limit = 0, $post = '', $cookie = '', $bysocket = FALSE, $ip = '', $timeout = 15, $block = TRUE, $encodetype  = 'URLENCODE', $allowcurl = TRUE) {
	$return = '';
	$matches = parse_url($url);
	$scheme = strtolower($matches['scheme']);
	$host = $matches['host'];
	$path = !empty($matches['path']) ? $matches['path'].(!empty($matches['query']) ? '?'.$matches['query'] : '') : '/';
	$port = !empty($matches['port']) ? $matches['port'] : ($scheme == 'https' ? 443 : 80);

	if(function_exists('curl_init') && function_exists('curl_exec') && $allowcurl) {
		$ch = curl_init();
		$ip && curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: ".$host));
		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		// 在请求主机名并非一个合法 IP 地址, 使用 CURLOPT_RESOLVE 设置固定的 IP 地址与域名关系
		if(!filter_var($host, FILTER_VALIDATE_IP)) {
			curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
			curl_setopt($ch, CURLOPT_RESOLVE, array("$host:$port:$ip"));
			curl_setopt($ch, CURLOPT_URL, $scheme.'://'.$host.':'.$port.$path);
		} else {
			curl_setopt($ch, CURLOPT_URL, $scheme.'://'.($ip ? $ip : $host).':'.$port.$path);
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if($post) {
			curl_setopt($ch, CURLOPT_POST, 1);
			if($encodetype == 'URLENCODE') {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			} else {
				parse_str($post, $postarray);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postarray);
			}
		}
		if($cookie) {
			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
		}
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$data = curl_exec($ch);
		$status = curl_getinfo($ch);
		$errno = curl_errno($ch);
		curl_close($ch);
		if($errno || $status['http_code'] != 200) {
			return;
		} else {
			return !$limit ? $data : substr($data, 0, $limit);
		}
	}

	if($post) {
		$out = "POST $path HTTP/1.0\r\n";
		$header = "Accept: */*\r\n";
		$header .= "Accept-Language: zh-cn\r\n";
		if($allowcurl) {
			$encodetype = 'URLENCODE';
		}
		$boundary = $encodetype == 'URLENCODE' ? '' : '; boundary='.trim(substr(trim($post), 2, strpos(trim($post), "\n") - 2));
		$header .= $encodetype == 'URLENCODE' ? "Content-Type: application/x-www-form-urlencoded\r\n" : "Content-Type: multipart/form-data$boundary\r\n";
		$header .= "User-Agent: {$_SERVER['HTTP_USER_AGENT']}\r\n";
		$header .= "Host: $host:$port\r\n";
		$header .= 'Content-Length: '.strlen($post)."\r\n";
		$header .= "Connection: Close\r\n";
		$header .= "Cache-Control: no-cache\r\n";
		$header .= "Cookie: $cookie\r\n\r\n";
		$out .= $header.$post;
	} else {
		$out = "GET $path HTTP/1.0\r\n";
		$header = "Accept: */*\r\n";
		$header .= "Accept-Language: zh-cn\r\n";
		$header .= "User-Agent: {$_SERVER['HTTP_USER_AGENT']}\r\n";
		$header .= "Host: $host:$port\r\n";
		$header .= "Connection: Close\r\n";
		$header .= "Cookie: $cookie\r\n\r\n";
		$out .= $header;
	}

	$fpflag = 0;
	$context = array();
	if($scheme == 'https') {
		$context['ssl'] = array(
			'verify_peer' => false,
			'verify_peer_name' => false,
			'peer_name' => $host,
			'SNI_enabled' => true,
			'SNI_server_name' => $host
		);
	}
	if(ini_get('allow_url_fopen')) {
		$context['http'] = array(
			'method' => $post ? 'POST' : 'GET',
			'header' => $header,
			'timeout' => $timeout
		);
		if($post) {
			$context['http']['content'] = $post;
		}
		$context = stream_context_create($context);
		$fp = @fopen($scheme.'://'.($ip ? $ip : $host).':'.$port.$path, 'b', false, $context);
		$fpflag = 1;
	} elseif(function_exists('stream_socket_client')) {
		$context = stream_context_create($context);
		$fp = @stream_socket_client(($scheme == 'https' ? 'ssl://' : '').($ip ? $ip : $host).':'.$port, $errno, $errstr, $timeout, STREAM_CLIENT_CONNECT, $context);
	} else {
		$fp = @fsocketopen(($scheme == 'https' ? 'ssl://' : '').($scheme == 'https' ? $host : ($ip ? $ip : $host)), $port, $errno, $errstr, $timeout);
	}

	if(!$fp) {
		return '';
	} else {
		stream_set_blocking($fp, $block);
		stream_set_timeout($fp, $timeout);
		if(!$fpflag) {
			@fwrite($fp, $out);
		}
		$status = stream_get_meta_data($fp);
		if(!$status['timed_out']) {
			while (!feof($fp) && !$fpflag) {
				if(($header = @fgets($fp)) && ($header == "\r\n" ||  $header == "\n")) {
					break;
				}
			}

			$stop = false;
			while(!feof($fp) && !$stop) {
				$data = fread($fp, ($limit == 0 || $limit > 8192 ? 8192 : $limit));
				$return .= $data;
				if($limit) {
					$limit -= strlen($data);
					$stop = $limit <= 0;
				}
			}
		}
		@fclose($fp);
		return $return;
	}
}

function fsocketopen($hostname, $port = 80, &$errno = null, &$errstr = null, $timeout = 15) {
	$fp = '';
	if(function_exists('fsockopen')) {
		$fp = @fsockopen($hostname, $port, $errno, $errstr, $timeout);
	} elseif(function_exists('pfsockopen')) {
		$fp = @pfsockopen($hostname, $port, $errno, $errstr, $timeout);
	} elseif(function_exists('stream_socket_client')) {
		$fp = @stream_socket_client($hostname.':'.$port, $errno, $errstr, $timeout);
	}
	return $fp;
}

function encode_file($filename) {
	if (file_exists($filename)) {
		$i = pathinfo($filename);
		if (in_array($i['extension'], array('js', 'css', 'php', 'html', 'htm'))) {
			$res = file_get_contents($filename);
			// 有的地方说 EUC-CN = GB2312，CP936 = GBK，这里都写上
			$encode = mb_detect_encoding($res, array("ASCII", "UTF-8", "GB2312", "GBK", "EUC-CN", "CP936", "GB18030", "BIG-5"));
			if (in_array($encode, array("GB2312", "GBK", "EUC-CN", "CP936", "GB18030", "BIG-5"))) {
				$res = mb_convert_encoding($res, "UTF-8", $encode);
				// 对非 Windows 系统尝试设置 777 权限
				if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
					logmessage("set chmod 777 $filename");
					chmod($filename, 0777);
				}
				logmessage("file convert $filename");
				file_put_contents($filename, $res);
			}
		}
	}
}

function encode_tree($directory) {
	$tdir = dir($directory);
	while ($file = $tdir->read()) {
		if ((is_dir("$directory/$file")) && ($file != ".") && ($file != "..")) {
			encode_tree("$directory/$file");
		} else {
			$file ="$directory/$file";
			if (is_file($file)) {
				encode_file($file);
			}
		}
	}
}

function cssoutput() {
	header('Content-Type: text/css; charset=utf-8');
	$data = base64_decode('H4sIAAAAAAACA61ZCXOrOBL+K6ynUk52EAEc7ATX3vd917G3QI3RBBAl5Ngeyv99JCRjYWzyrpn3DGr1pa9brRbv+24c40wAl88EMsahTdgeNfRbWm3ihHECHEnKMRdl0WasEijDJS0OcYOrBjXAabYuaAUoB7rJRRx4QbRGO0heqUAC9kLpAoTJN9tGzvr+3XkW1yiXQoUSRCkrGI8Fl2przKESx4wx5VgOmMhHiWnlVvitJbSpC3yIk4Klr8eEkUNbYr6hVeyvbQcRrusCUHNoBJTuT6WTr3/A6d+64S8lnzv7G2wYOP/4zcz9K0uYYO7s11C8gaApdv4IW5i5P+EUF+7sj3LS+Zv0bObOfk8T4FhQVhnKGQh39hNl0vmZWorzi5J9Q2dnK2PC3w5lwoqZ0W9L6YUo5OKAQ6mHO43wk+9fIB6tNXhfhUEYhS/rDnYsYa3iAjKxTnD6uuFsWxFkGLMsO+aBwQ0JVkvszEACIVgZe5G0e6ynWALFgQnh0DTteMqsQRwKiCvGS1wM3KZVDpyK47Z418a2cDSXNeEfk9ZGRQJJgB9xa1bo+6skyzQUBFKmQyYdqWCMh511OM7ZG/CznmiZLEZ6pCxwtRzJXzFx/6+cQ/afB/2eFrhp/vNgVJiFXnFlUtp2Y1pHygi4NYfB9vzbL//AKob+Cpttgbn7B6gK5koSTpn7M1Y1TFoZJLNil6n4M7blFLjM/93MLSVN4pKCnY8qLTi8F7S18j4r2C7GW8HWqGzQiWJyokk5K4oE8yMtN62cU/uuMHlbUkIKWOsCpAX0YgVOCmgNPZUKcN1AbF7kdN5a2W/DZkin4lNikebIxDzZSr+rk1aOCd02sW/IccZSOVSx0a/ojTZUevHQsq1QOdCzXi9ExgsLQ0O5uh+Mph4+Y8zQ9eq6fM0YLzUm/5JAwg80w3/adMsbxuOa0UoAP/5LHOp+0jVaDAqqRAKWylKI9cyQXS+a0EaBTh6M9AV10mAco5J9izRutKqAGyXjibbGhKhTx78S9rgPXEYLQNu6YJggsxoFbI/p7aXJaNVb0Z8ftOrg18eIgFISBZxmtVUvD1xTJu3KeFmRo1FFDk+i55iHuqJ6BWBikSXvSN/C97tNbbE9r7zo7lTm4XkBz+l6J1FCO47rOOGAX5EaH/EPleSwbKj9Ojw212PN1gGi+J1OzThlB4q1C511U+KPXipFMFXx3FEicn3km9AirvGJ6n1PKiAzFA2z5tFVQ1M0iyIcf1wCodi5LyVVq49Wy3r/0FpmS7w/zT359f54RWi1fL4ptApvCL28hDeFXpY3hILQ929KBYF20ONs18cnK2C/Vj86tOpniAyywVLQaIpCvmhr1tDuZOBQYEHfYP2RUdBqOvsJbmgT+9qZjfQxDtbGeaPx6OlybNkYnwTD3DIijiDu6S3vd7636vbCxVkgWG0qQnfaBPXeaVhBifMVAQhh2atUnaJSdyGvfTmpMJ6FN7WojvLr7re1rN7iR43gtAai5RzB40rkiGVIVcF7RmR9HLUbfJPge99V/3t+9HD0ElFdLUujvu9mn5eCqr3r66fotlHFFApIxfsNkMHJgtme7CO16ELlmIgN29VRezo8WE3FU2pNsnZOOF4QNQ7gBhCtENsK13bzOodWfHt2j5ocE7YbzfVbteaQAW8QB7JNgaCSdR7p4UPbhab31DRcXcDs/syOyLg/U9y6czg3DOuzb7HvqP+9UOFpUiMIF24YRa4XmvToj9uW1Til4hB7y0jPDE/jbujdPJyVBKo5LTE/tOfLwCgj+gbaQrgnDrRYOEzoWr6Ql5GuZZimQ10apU/VNQnq4tkNngyqClTLbI/el0FkOiIxTlVdnjal1zMyFaXJJ5gyqH4sOA2krCLv5skyXUUrYrvaE209H5gpEV6Gy+cLbdFTlCzDS216VZ+qbTJXgoXcgh0mIzzsdPliwHx6wowWZRt7giiKEtvYl0uZMUanO5jW2ORs93DRTVf4bbLNsZuR0aVStdiN+Z5w1ofUZ52L3tao0Y26ow6kM6temNuPTU6O67bRn2B+paeaWkR38iIqoGxOp7L69EWzA1I9oKTE3ZUaJSB2ANVVd7Vlx+oav6TJk36UcFyRUedhg9j1Pt4i0Ee2IZqQnOl2f6o7gNEF59pdd73LqQDU+SYhV2sZutZHy6a9HzF0PdEI5ZCe+o1tWX1Kvhntzjn3hk21f6G0FzrtDqu11g2z3V2PA9nLC7bZFNYl2Qvt/msa7uDTG76rrdulU3aYLOp0oGxWRFN2owM2Vwsvkmsyy9GD663uKd9nM2vNceTfPSqwne6nYohDDfjcBFq3v+XKe9GXQ+Mi7GssYSvJD63dOB30iSvnSKvOh+5LD2c7R++C0fbtmBqBuTiONDh21g8TXamcEJhIZF2N7HUZ0nESl3Nh0guZMN5vCHujfo+WNeMCy8Sz9om6+k9oMkk0PGx6/kItxxlWvPE17OVhSsAk+BSHTvb3Fd8AfywYfZCg7dk0500PV7cMGVyv+tZ3GqO5YFqd2etWQaIl3kC85cX9jGCB42782Lxtvt6XhXu3SOWrI1+r5gfzXIg6fnzc7XbebuExvnkMfd9XzHOn22c/mC/8uaMLhX5/o7D7Kdv/YK76l4X6M79bgFRbY5E7jeDsFX4wVyu4C5991zF/vOgufJmbeQUgpLiWfMrlnlxSAbygJZW2Ar8nG0/CuUN+MP/Dk7PKw1A+gkg/w4V8zh+1F8p3+TZTTeZGI9aO27vsOXvJ8Hl3HHHPbTLAInSBNp9ZL9gsqsV7xSDB4ENkGfR0wFv9GH+EsVkJUpt3vLHNtBojfQS3Vw/mEXOTc5nByG+tUezbbDmSpb3Vce/OVXuyFCiy/71iwaG05+sDWrR2wxNMMVifti6MqDpl21HjAQu3WXShHTF152W5FUDawZ3B4vkO2wSYX7UdAAA=');
	if(empty($_SERVER['HTTP_ACCEPT_ENCODING']) || strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === false) {
		echo gzdecode($data);
	} else {
		header('Content-Encoding: gzip');
		echo $data;
	}
	exit;
}

function logmessage($message, $close = false) {
	static $fh = false;
	if(!$fh) {
		$fh = fopen(UPGRADE_LOG_PATH, "a+");
	} 
	if($fh) {
		fwrite($fh, $message."\r\n");
		fflush($fh);
		if($close) {
			fclose($fh);
		}
	}
}

function setdbglobal($db) {
	// 对于因数据库超时而升级失败的特大站点，请赋予升级程序所使用的 MySQL 账户 SUPER privilege 权限，并解除以下三行代码注释后再试
	// $db->query('SET GLOBAL connect_timeout=28800');
	// $db->query('SET GLOBAL wait_timeout=28800');
	// $db->query('SET GLOBAL interactive_timeout=28800');
}

function timezone_set($timeoffset = 8) {
	if(function_exists('date_default_timezone_set')) {
		@date_default_timezone_set('Etc/GMT'.($timeoffset > 0 ? '-' : '+').(abs($timeoffset)));
	}
}